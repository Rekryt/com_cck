<?php
class CCKORMTests extends CCKORModel {
	public $name_table 			= 'tests';	//имя таблицы
	public $name_entities 		= 'tests';	//имя сущности
	public $name_entity 		= 'test';	//имя экземпляра сущности

	public $params 	= [
		'items'=>[	//параметры страницы списка элементов
			'title'=>'Тестовый элемент',	//заголовок раздела управления списком элементов
			'list'=>[	//список полей
				'id'				=>	['field'=>'id','sort'=>true,'link'=>true,'width'=>'1%'],
				'photo'				=>  ['field'=>'photo','sort'=>false,'link'=>true,'type'=>'photo'],
				'name_ru'			=>	['field'=>'name_ru','sort'=>true,'link'=>true],
				'categories_id'		=>	['field'=>'categories_name_ru','sort'=>true,'sort_field'=>'categories.name_ru'],
				'state'				=>	['field'=>'state','sort'=>true,'type'=>'checker','width'=>'1%'],
				'date_create'		=> 	['field'=>'date_create','sort'=>true],
				'ordering'			=>	['field'=>'ordering','sort'=>true,'type'=>'text','width'=>'1%']
			],
			'order'=>'tests.id',	//начальная сортировка
			'dir'=>'ASC',	//начальное направление сортировки
			'filters'=>[	//фильтры
				'search'=>[	//фильтр поиска
					'type'=>'search',	//тип фильтра search - текстовое поле поиска
					'field'=>['name_ru','name_en'],	//список полей по которым проводится поиск
					'label'=>"Поиск"	//заголовок поля
				],
				'state'=>[	//фильтр состояния публикации
					'type'=>'select',	//тип фильтра
					'field'=>'state',	//поле фильтрации
					'label'=>"- Выбор состояния -",	//заголовок поля
					'options'=>[	//список параметров
						'1'=>'Опубликовано',
						'0'=>'Не опубликовано'
					]
				],
				'categories_id'=>[	//фильтр категории
					'type'=>'select',	//тип фильтра
					'field'=>'categories_id',	//поле фильтрации
					'label'=>"- Выбор категории -",	//заголовок поля
					'source'=>[	//источник данных
						'entity'=>'categories',	//имя сущности источника
						'field'=>'id',	//поле со значением опции фильтра
						'value'=>'name_ru'	//поле с наименованием опции фильтра
					]
				]
			],
			'toolbar'=>[	//верхняя панель управления
				[
					'type'=>'add',	//тип элемента управления
					'label'=>'Создать'	//наименование элемента управления
				], [
					'type'=>'edit',
					'label'=>'Создать'
				], [
					'type'=>'check',	//тип check - устанавливает значение поля как '1'
					'label'=>'Опублировать',	//наименование элемента управления
					'field'=>'state'	//имя поля управления
				], [
					'type'=>'uncheck',	//тип uncheck - устанавливает значение поля как '0'
					'label'=>'Снять с публикации',	//наименование элемента управления
					'field'=>'state'	//имя поля управления
				], [
					'type'=>'delete',
					'label'=>'Удалить'
				]
			]
		],
		'item'=>[	//параметры страницы элемента
			'title'=>'Тест',	//заголовок
			'form'=>[	//поля формы
				'Общее'				=>	['id', 'categories_id', 'name_ru', 'pseudoname_ru', 'name_en', 'pseudoname_en', 'tags_id', 'state', 'date_create', 'date_modify'],
				'Текст страницы'	=>	['photo', 'text_ru', 'text_en']
			]
		],
		'menu'=>[ 	//параметры субменю
			'order' => 0,
			'hidden' => true
		]
	];

	function __construct($if_not_exists = false) {
		$this->addFields([
			'id'=>[
				'params'		=>	[
					'label'			=>	'ID',
					'type'			=>	'hidden'
				],
				'type'			=>	'int(11)',
				'null'			=>	'NOT NULL',
				'default'		=>	'',
				'autoincrement'	=>	true
			],
			'categories_id'=>[
				'params'		=>	[
					'label'			=>	'Категория статьи',
					'type'			=>	'select',
					'source'=>[
						'entity'=>'categories',
						'field'=>'id',
						'value'=>'name_ru'
					]
				],
				'type'			=>	'int(11)',
				'null'			=>	'NOT NULL',
				'default'		=>	'0',
				'relation'	=> [
					'entity'		=> 'categories',	//сущность с которой делается связь 1-м
					'field'			=> 'id',	//ключевое поле из сущности соответсвующее значению (categories_id) текущей сущности
					'value' 		=> ['name_ru','name_en']	//поля (первое то что берём для отображения)
				]
			],
			'tags_id'=>[
				'params'		=>	[
					'label'			=>	'Теги',
					'type'			=>	'multipleSelect',
					'source'=>[
						'entity'=>'tags',
						'field'=>'id',
						'value'=>'name_ru'
					]
				],
				'relation'	=> [
					'entity'				=> 'tags', 			//сущность с коротой делается связь (tags)
					'field'					=> 'id', 			//ключевое после этой сущности (tags.id)
					'multiple_field' 		=> 'tags_id',		//соответствующее поле из вспомогательной таблицы (articles_tags.tags_id)
					'multiple_entity'		=> 'tests_tags', //имя вспомогательной таблицы (articles_tags)
					'multiple_entity_field'	=> 'tests_id',	//ключевое поле вспомогательной таблицы для связи м-м (articles_tags.articles_id)
					'multiple_key'			=> 'id',			//соответствующее ключевое из текущей сущности (articles.id)
					'value' 				=> ['name_ru','name_en']		//поле из которого берём отображаемое значение (tags.name)
				]
			],
			'state'=>[
				'params'		=>	[
					'label'			=>	'JSTATUS',
					'type'			=>	'list',
					'class'			=> 	'inputbox',
					'size'			=> 	'1',
					'default' 		=> 	'1',
					'options' 		=> 	[
						'1'				=> 	'Опубликовано',
						'0'				=> 	'Не опубликовано'
					]
				],
				'type'			=>	'tinyint(3)',
				'null'			=>	'NOT NULL',
				'default'		=>	'1'
			],
			'ordering'=>[
				'params'		=>	[
					'label'			=>	'Порядок',
					'type'			=>	'hidden'
				],
				'type'			=>	'int(11)',
				'null'			=>	'NOT NULL',
				'default'		=>	'1'
			],
			'name_ru'=>[
				'params'		=>	[
					'label'			=>	'Наименование (RU)',
					'type'			=>	'text',
					'size'			=>	'60',
					'default'		=>	'',
					'required'		=>	true
				],
				'type'			=>	'varchar(255)',
				'null'			=>	'NOT NULL',
				'default'		=>	''
			],
			'pseudoname_ru'=>[
				'params'		=>	[
					'label'			=>	'URL псевдоним (RU)',
					'type'			=>	'text',
					'size'			=>	'60',
					'default'		=>	'',
					'description'	=>	'Генерируется при сохранении автоматически',
					'extra'			=> 	['update'=>'pseudoname', 'field'=>'name_ru']
				],
				'type'			=>	'varchar(255)',
				'null'			=>	'NOT NULL',
				'default'		=>	''
			],
			'name_en'=>[
				'params'		=>	[
					'label'			=>	'Наименование (EN)',
					'type'			=>	'text',
					'size'			=>	'60',
					'default'		=>	'',
					'required'		=>	true
				],
				'type'			=>	'varchar(255)',
				'null'			=>	'NOT NULL',
				'default'		=>	''
			],
			'pseudoname_en'=>[
				'params'		=>	[
					'label'			=>	'URL псевдоним (EN)',
					'type'			=>	'text',
					'size'			=>	'60',
					'default'		=>	'',
					'description'	=>	'Генерируется при сохранении автоматически',
					'extra'			=> 	['update'=>'pseudoname', 'field'=>'name_en']
				],
				'type'			=>	'varchar(255)',
				'null'			=>	'NOT NULL',
				'default'		=>	''
			],
			'photo'=>[
				'params'		=>	[
					'type'			=>	'media',
					'label'			=>	'Фотография',
					'directory'		=>	''
				],
				'type'			=>	'varchar(255)',
				'null'			=>	'NOT NULL',
				'default'		=>	''
			],
			'text_ru'=>[
				'params'		=>	[
					'label'			=>	'Текст страницы (RU)',
					'type'			=>	"editor",
					'filter'		=>	"raw",
					'buttons'		=>	false,
					'height'		=>	300,
					'width'			=>	750
				],
				'type'			=>	'text',
				'null'			=>	'NULL',
				'default'		=>	'NULL'
			],
			'text_en'=>[
				'params'		=>	[
					'label'			=>	'Текст страницы (EN)',
					'type'			=>	"editor",
					'filter'		=>	"raw",
					'buttons'		=>	false,
					'height'		=>	300,
					'width'			=>	750
				],
				'type'			=>	'text',
				'null'			=>	'NULL',
				'default'		=>	'NULL'
			],
			'date_create'=>[
				'params'		=>	[
					'label'			=>	'Дата создания',
					'type'			=>	'fullDate'
				],
				'type'			=>	'timestamp',
				'null'			=>	'NOT NULL',
				'default'		=>	'CURRENT_TIMESTAMP'
			],
			'date_modify'=>[
				'params'		=>	[
					'label'			=>	'Дата последней модификации',
					'type'			=>	'raw',
					'extra'			=> 	['update'=>'current_timestamp']
				],
				'type'			=>	'timestamp',
				'null'			=>	'NOT NULL',
				'default'		=>	'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
			]
		]);
		$this->addKeys([
			'id'=>[
				'type'=>'PRIMARY'
			],
			'idx_state_ru'=>[
				'fields'=>['state_ru']
			],
			'idx_state_en'=>[
				'fields'=>['state_en']
			],
			'idx_categories_id'=>[
				'fields'=>['categories_id']
			],
			'uni_pseudoname_ru'=>[
				'type'=>'UNIQUE',
				'fields'=>['pseudoname_ru']
			],
			'uni_pseudoname_en'=>[
				'type'=>'UNIQUE',
				'fields'=>['pseudoname_en']
			]
		]);
		return parent::__construct();
	}
}