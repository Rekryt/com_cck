<?php
defined('_JEXEC') or die('Restricted access');

// Используем route.php для создания ссылок на контент
require_once (JPATH_SITE.'/'.'components'.'/'.'com_cck'.'/'.'router.php'); //vb helper

class modCCKContentHelper {
	public $orm;
	function __construct($entity = '') {
		$this->orm = getORM($entity);
	}

	function get($params, $filter = []) {
        $db = JFactory::getDBO();
		$query  = $db->getQuery(true);
		$alias = $this->orm->name_entities;
		//defaults
		foreach (array("$alias."."state".langprefix()=>1) as $key=>$val) if (!isset($filter[$key])) $filter[$key] = $val;

		// Выбираем нужные поля из таблицы
		if (isset($filter['fields']) && is_array($filter['fields'])) {
			foreach ($filter['fields'] as $field) $query->select("$alias.".$field);
		} else {
			$query->select("$alias.*");
		}

		$query->from('#__cck_'.$this->orm->name_table." AS $alias");

		//relations
		foreach ($this->orm->relations as $key=>$relation) {
			if (!$relation['list']) continue;
			$p = $relation['entity'];
			if (isset($relation['multiple_entity'])) {
				$query->leftJoin('#__cck_'.$relation['multiple_entity']." rel_$p ON rel_$p.".$relation['multiple_entity_field']." = a.".$relation['multiple_key']);
				$query->leftJoin('#__cck_'.$relation['entity']." $p ON $p.".$relation['field']." = rel_$p.".$relation['multiple_field']);
			} else {
				$query->leftJoin('#__cck_'.$relation['entity']." $p ON $p.".$relation['field']." = ".$alias.".".$key);
			}
			foreach ($relation['value'] as $item)
				$query->select("$p.".$item." as ".$relation['entity']."_".$item);
		}

		//filters
		foreach ($filter as $key=>$val) {
			switch ($key) {
				case "order":
					$query->order(addslashes($val));
				break;
				default:
					$query->where($key." = '".addslashes($val)."'");
				break;
			}
		}

		#limits
		if($params->get('limit')) $query->setLimit($params->get('limit'));

		$query->order($params->get('order')." ".$params->get('dir'));

		//if (!$count) dbg((string) $query, true);
		$db->setQuery($query);
		$rows = langfix($db->loadObjectlist());
		foreach ($rows as $row) {
			if (isset($relation)) {
				$row->href = getSEFlink(
					'index.php?option=com_cck&view=items&entity='.$alias."&".$relation['entity']."_".$relation['field']."=".$row->{$relation['entity']."_".$relation['field']},
					['view'=>'item', 'id'=>$row->id]
				);
			} else {
				$row->href = getSEFlink(
					'index.php?option=com_cck&view=items&entity='.$alias,
					['view'=>'item', 'id'=>$row->id]
				);
			}
		}
		return $rows;
    }
}