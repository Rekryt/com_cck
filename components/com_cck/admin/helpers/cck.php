<?php defined('_JEXEC') or die;

class CCKHelper {
	public static function addSubmenu($vName = 'et') {
		$user = JFactory::getUser();
		//if ($user->authorise('core.admin', 'com_cck'))

		//JHtmlSidebar::addEntry("Управление", 'index.php?option=com_cck&view=cck', $vName == 'cck');

		$menu = [];
		foreach (scandir(JPATH_COMPONENT.'/orm/') as $file) {
			$parts = explode(".", $file);
			if (!in_array($file, ['.', '..']) && array_pop($parts) == 'php') {
				$name = array_shift($parts);
				$orm = getORM($name);
				if ($name != 'default' && !(isset($orm->params['menu']['hidden']) && $orm->params['menu']['hidden']))
					$menu[] = [
						'entity'=>$name,
						'title'=>$orm->params['items']['title'],
						'order'=>isset($orm->params['menu']) ? $orm->params['menu']['order'] : 0
					];
				self::array_sort_by_column($menu, 'order');
			}
		}
		foreach ($menu as $item)
			JHtmlSidebar::addEntry($item['title'], 'index.php?option=com_cck&view=items&entity='.$item['entity'], $vName == $item['entity']);

		JHtmlSidebar::addEntry('&nbsp;', '', $vName == '');

		JHtmlSidebar::addEntry("Настройки", 'index.php?option=com_config&view=component&component=com_cck', $vName == 'settings');
	}

	function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
		$sort_col = array();
		foreach ($arr as $key=> $row) {
			$sort_col[$key] = $row[$col];
		}

		array_multisort($sort_col, $dir, $arr);
	}

  public static function getActions($messageId = 0) {
    $user  = JFactory::getUser();
    $result  = new JObject;
 
    if (empty($messageId)) {
      $assetName = 'com_cck';
    }
    else {
      $assetName = 'com_cck.message.'.(int) $messageId;
    }
 
    $actions = array(
      'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.delete'
    );
 
    foreach ($actions as $action) {
      $result->set($action, $user->authorise($action, $assetName));
    }
 
    return $result;
  }
  
  public function TransUrl($str)
  {
      $tr = array(
          "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g","Д"=>"d","Е"=>"e","Ё"=>"yo","Ж"=>"zh","З"=>"z","И"=>"i","Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n","О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t","У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch","Ш"=>"sh","Щ"=>"shch","Ъ"=>"","Ы"=>"y","Ь"=>"","Э"=>"e","Ю"=>"yu","Я"=>"ya",
          "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"zh","з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l","м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r","с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h","ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"shch",	"ъ"=>"","ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
          " "=>"-","-"=>"-",
          "A"=>"a","B"=>"b","C"=>"c","D"=>"d","E"=>"e","F"=>"f","G"=>"g","H"=>"h","I"=>"i","J"=>"j","K"=>"k","L"=>"l","M"=>"m","N"=>"n","O"=>"o","P"=>"p","Q"=>"q","R"=>"r","S"=>"s","T"=>"t","U"=>"u","V"=>"v","W"=>"w","X"=>"x","Y"=>"y","Z"=>"z",
          "a"=>"a","b"=>"b","c"=>"c","d"=>"d","e"=>"e","f"=>"f","g"=>"g","h"=>"h","i"=>"i","j"=>"j","k"=>"k","l"=>"l","m"=>"m","n"=>"n","o"=>"o","p"=>"p",
          "q"=>"q","r"=>"r","s"=>"s","t"=>"t","u"=>"u","v"=>"v","w"=>"w","x"=>"x","y"=>"y","z"=>"z",
          "0"=>"0","1"=>"1","2"=>"2","3"=>"3","4"=>"4","5"=>"5","6"=>"6","7"=>"7","8"=>"8","9"=>"9",
          "."=> "","/"=> "",","=>"","("=>"",")"=>"","["=>"","]"=>"","="=>"","+"=>"",
          "*"=>"","?"=>"","\""=>"","'"=>"","&"=>"","%"=>"","#"=>"","@"=>"","!"=>"",";"=>"","№"=>"n",
          "^"=>"",":"=>"","~"=>"","\\"=>"", "`"=>"", ":"=>"", "«"=>"", "»"=>"", "—"=>"-", "’"=>""
      );
      return str_replace("--", "-", str_replace("--", "-", str_replace("---", "-", strtr(strtolower($str),$tr))));
  }
  
}
