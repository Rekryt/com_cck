<?php defined('_JEXEC') or die;
 
class CCKControllerItems extends JControllerAdmin {
	public function getModel($name = 'Items', $prefix = 'CCKModel', $config = array('ignore_request' => true)){
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
	public function __construct(array $config = array()) {
		//ORM
		$this->orm = getORM();
		parent::__construct($config);
	}

	public function saveorder() {

		$input = JFactory::getApplication()->input;
		$pks = $input->post->get('cid', array(), 'array');
		$entity = $input->get('entity', '', 'string');

		JArrayHelper::toInteger($pks);
		//JArrayHelper::toInteger($order);
		$update = [];
		foreach ($this->orm->getList() as $key=>$field) {
			if(isset($field['type']) && $field['type'] == 'text') {
				if (!isset($update[$field['field']])) $update[$field['field']] = [];
				foreach ($input->post->get($field['field'], [], 'array') as $i=>$data)
					$update[$field['field']][$pks[$i]] = $data;
			}
		}

		// Получаем модель
		$model = $this->getModel();

		// Сохранить
		if($model->saveorder($entity, $update)) {
			JError::raiseNotice(100,"Данные сохранены");
		}
		$this->setRedirect(JRoute::_('index.php?option=com_cck&view=items&entity='.$entity, false));
		return;
	}

	public function check() {
		$entity = $this->input->get('entity', '', 'string');
		$field = addslashes(array_pop(explode(".", $_POST['task'])));
		$pks = $this->input->get('cid', array(), 'array');

		// Сохранить
		$model = $this->getModel();
		foreach ($pks as $id)
			$model->update($entity, $field, $id, '1');

		JError::raiseNotice(100,"Данные сохранены");
		$this->setRedirect(JRoute::_('index.php?option=com_cck&view=items&entity='.$entity, false));
		return;
	}

	public function uncheck() {
		$entity = $this->input->get('entity', '', 'string');
		$field = addslashes(array_pop(explode(".", $_POST['task'])));
		$pks = $this->input->get('cid', array(), 'array');

		// Сохранить
		$model = $this->getModel();

		foreach ($pks as $id)
			$model->update($entity, $field, $id, '0');

		JError::raiseNotice(100,"Данные сохранены");
		$this->setRedirect(JRoute::_('index.php?option=com_cck&view=items&entity='.$entity, false));
		return;
	}

	public function superdelete() {
		$db = JFactory::getDBO();
		$entity = $this->input->get('entity', '', 'string');
		//dbg($this->orm->create(), true);
		$db->setQuery("DROP TABLE IF EXISTS `#__cck_".$entity."`");
		$db->query();

		foreach ($this->orm->relations as $field=>$relation) {
			if ($relation['multiple_entity']) {
				$db->setQuery("DROP TABLE IF EXISTS `#__cck_".$relation['multiple_entity']."`");
				$db->query();
			}
		}

		foreach ($this->orm->create() as $table) {
			$db->setQuery($table);
			$db->query();
			JError::raiseNotice(100,"<pre>$table</pre>");
		}

		JError::raiseNotice(100,"Сущность пересоздана");
		$this->setRedirect(JRoute::_('index.php?option=com_cck&view=items&entity='.$entity, false));
		return;
	}

}
