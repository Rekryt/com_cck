<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

$lang = JFactory::getLanguage();

require_once JPATH_ROOT.'/components/com_cck/lib/functions.php';
require_once dirname(__FILE__) . '/helper.php';

$items = modCCKMenuHelper::getItems(array("t.id"=>$params->get('menu_types_id')));

$layout = $params->get('layout', 'default');
require(JModuleHelper::getLayoutPath('mod_cck_menu', $layout));