<?php defined('_JEXEC') or die;

class CCKControllerItem extends CCKController {
	function display($cachable = false, $urlparams = array()) {
		$helper = new CCKHelper();
		$entity = $this->input->get('entity');
		$active = JFactory::getApplication()->getMenu()->getActive();

		$view = $this->getView('item', 'html');

		$view->setLayout($active->params->get('layout', $entity));

		$model = $this->getModel('items');
		$item = $model->get([$entity.'.id'=>$this->input->get('id', $this->input->get('pages_id'))])[0];
		$view->assign('item', $item);

		$helper->setMetaData([
			"title"=>$item->title ? $item->title : $item->name,
			"description"=>$item->description,
			"og:title"=>$item->title ? $item->title : $item->name,
			"og:description"=>$item->description,
			"og:url"=>JURI::current(),
			"og:type"=>"website"
		]);

		if (isset($item->image) && $item->image) {
			$thumb = generateThumb($item->image, '800', '');
			$helper->setMetaData([
				"og:image"=>$thumb['thumb_href']
			]);
		}

		parent::display($cachable, $urlparams);
		//JFactory::getApplication()->close();
	}
}
