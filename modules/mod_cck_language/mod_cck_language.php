<?php
defined('_JEXEC') or die('Restricted access');
require_once(dirname(__FILE__).'/'.'helper.php'); // Подключаем локальный helper
//require_once JPATH_SITE.'/components/com_vp/lib/functions.php';

$lang = JFactory::getLanguage();
$lang->load('com_cck');
$input = JFactory::getApplication()->input;
$document   = JFactory::getDocument();
$canonical_url = getRefURL();
$canonical_altername = getRefURL(array("lang"=>langprefix() == "_ru" ? 'en-GB' : 'ru-RU'));

$base_uri = $_SERVER['SERVER_NAME'];
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
	$base_uri = "https://".$base_uri;
} else {
	$base_uri = "http://".$base_uri;
}

$lang = substr(langprefix(), 1);
$link = [];

if ($input->get('view') == 'item') {
	$entity = $input->get('entity');
	$orm = getORM($entity);

	$item = modCCKLanguageHelper::get($entity, $input->get('id', $input->get('pages_id')))[0];

	$link['ru'] = $item->state_en ? getRefURL(['lang'=>'en-GB']) : '/en';
	$link['en'] = $item->state_ru ? getRefURL(['lang'=>'ru-RU']) : '/ru';

	$document->addCustomTag('<link rel="canonical" href="'.$base_uri.parse_url($canonical_url)['path'].'">');
	if ($item->{'state'.langprefix(true)}) {
		$document->addCustomTag('<link rel="alternate" href="'.$base_uri.parse_url($canonical_altername)['path'].'" hreflang="'.substr(langprefix(), 1).'">');
	}
	//dbg([$link,$item], true);
} else {
	$link['ru'] = getRefURL(langprefix() == "_ru" ? ['lang'=>'en-GB'] : []);
	$link['en'] = getRefURL(langprefix() == "_en" ? ['lang'=>'ru-RU'] : []);

	$document->addCustomTag('<link rel="canonical" href="'.$base_uri.parse_url($canonical_url)['path'].'">');
	$document->addCustomTag('<link rel="alternate" href="'.$base_uri.parse_url($canonical_altername)['path'].'" hreflang="'.substr(langprefix(), 1).'">');
}

$layout = $params->get('layout', 'default');
// подключаем файл шаблона с помощью класса JModuleHelper
require(JModuleHelper::getLayoutPath('mod_cck_language', $layout));