<?php defined('_JEXEC') or die;

class CCKViewItems extends JViewLegacy {
	protected $items;
	protected $pagination;
	protected $state;
	protected $fields;
	private $orm;
	public $entity;
	public $params;

	public function __construct(array $config = array()) {
		//ORM
		$this->orm = getORM();
		parent::__construct($config);
	}

	public function display($tpl = null) {
		$this->state    = $this->get('State');
		$this->items    = $this->get('Items');
		$this->pagination  = $this->get('Pagination');
		$this->fields = $this->orm->params['items']['list'];
		$this->entity = $this->orm->name_entities;
		$this->params = $params = JComponentHelper::getParams('com_cck');
		CCKHelper::addSubmenu($this->orm->name_entities);

		// Проверить на наличие ошибок.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		//Добавление навигации
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}
	/**
	 * Добавить заголовок страницы и панели инструментов.
	 *
	 * @since  1.6
	 */
	protected function addToolbar() {
		$params = $this->orm->params['items'];
		//Вывод навигации
		require_once JPATH_COMPONENT.'/helpers/cck.php';
		
		//Фильтры публикации
		$state  = $this->get('State');
		$canDo  = CCKHelper::getActions();
		//echo "<pre>".print_r($canDo, true)."</pre>"; exit();
		$user  = JFactory::getUser();
		// Получить экземпляр объекта панели инструментов
		$bar = JToolBar::getInstance('toolbar');

		//Вывод панели инструментов, добавить, удалить, опубликовать, снять с публикации
		JToolbarHelper::title($params['title'], 'mycomponents.png');
		/*if ($canDo->get('core.create')) {
			JToolbarHelper::addNew('item.add');
		}
		if ($canDo->get('core.edit')) {
			JToolbarHelper::editList('item.edit');
		}*/

		foreach ($this->orm->params['items']['toolbar'] as $item) {
			switch ($item['type']) {
				case 'add':
					JToolbarHelper::addNew('item.add');
				break;
				case 'edit':
					JToolbarHelper::editList('item.edit');
				break;
				case 'check':
					JToolbarHelper::publish('items.check.'.$item['field'], $item['label'], true);
				break;
				case 'uncheck':
					JToolbarHelper::unpublish('items.uncheck.'.$item['field'], $item['label'], true);
				break;
				case 'delete':
					JToolbarHelper::deleteList('', 'items.delete', $item['label']);
				break;
			}
		}

		if ($this->params->get('programmer_mode', 0)) {
			JToolBarHelper::custom('items.superdelete', 'delete', 'delete', 'Удалить все данные', false);
		}

		JHtmlSidebar::setAction('index.php?option=com_sp&view=items&entity='.$this->entity);

		$model = $this->getModel();
		foreach ($this->orm->params['items']['filters'] as $field=>$filter) {
			if ($filter['type'] == 'select') {
				$options = [];
				if (isset($filter['options']))
					foreach ($filter['options'] as $value=>$text)
						$options[] = JHtml::_('select.option', $value, $text);

				if (isset($filter['source']))
					$options = array_merge($options, $model->source($filter['source']));

				JHtmlSidebar::addFilter(
					JText::_($filter['label']),
					'filter_'.$this->orm->name_entities.'_'.$field,
					JHtml::_('select.options', $options, 'value', 'text', $this->state->get('filter.'.$this->orm->name_entities.'_'.$field), true)
				);
			}

		}
	}

	/**
	 * Возвращает массив полей таблицы можно сортировать
	 *
	 * Возвращение массива, содержащий имя поля для сортировки в качестве ключа и текст на дисплее в качестве значения
	 *
	 * @since   3.0
	 */
	protected function getSortFields() {
		$fields = [];
		foreach ($this->orm->params['items']['list'] as $field)
			$fields[$field['field']] = JText::_($field['label']);

		return $fields;
	}
}