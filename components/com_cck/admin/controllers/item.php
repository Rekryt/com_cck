<?php defined('_JEXEC') or die;

class CCKControllerItem extends JControllerForm {
	private $orm;

	public function __construct($config = array()) {
		//ORM
		$this->orm = getORM();
		parent::__construct($config);
	}

	//Метод переопределения, что бы проверить можно ли добавить новую запись.
	public function batch($model = null) {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$model = $this->getModel('Machine', '', array()); // Установить модель
		$this->setRedirect(JRoute::_('index.php?option=com_cck&view=item' . $this->getRedirectToListAppend(), false)); // Предустановленная переадресация

		return parent::batch($model);
	}

	public function error_relay($jform, $message) {
	    $app            = JFactory::getApplication();
	    $tmpl           = JRequest::getString('tmpl');
	    $layout         = JRequest::getString('layout', 'edit');
	
	    if ($jform['id']) $append .= '&id='.$jform['id'];
	
	    // Setup redirect info.
	    if ($tmpl) {
	        $append .= '&tmpl='.$tmpl;
	    }
	    if ($layout) {
	        $append .= '&layout='.$layout;
	    }
	
	    $context = "$this->option.edit.$this->context";
	
	    // Save the data in the session.
	    $app->setUserState($context.'.data', $jform);
	
	    // Redirect back to the edit screen.
	    $this->setRedirect(JRoute::_('index.php?option='.$this->option.'&view='.$this->view_item.$append, false), $message, 'error');
	    return false;
	
	}
	public function add() {
		$entity = $this->input->get('entity', '', 'string');
		$this->setRedirect(JRoute::_('index.php?option=com_cck&view=item&entity='.$entity."&layout=edit", false));
		return true;
	}
	public function edit($key = NULL, $urlVar = NULL) {
		$id = $this->input->get('id', '', 'integer');
		$entity = $this->input->get('entity', '', 'string');
		$this->setRedirect(JRoute::_('index.php?option=com_cck&view=item&entity='.$entity."&layout=edit&id=".$id, false));
		return true;
	}
	public function cancel($key = NULL) {
		$entity = $this->input->get('entity', '', 'string');
		$this->setRedirect(JRoute::_('index.php?option=com_cck&view=items&entity='.$entity, false));
		return true;
	}

	public function save($key = NULL, $urlVar = NULL) {
	    JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$db = JFactory::getDBO();
	    $task = $this->input->get('task', '', 'string');
		$entity = $this->input->get('entity', '', 'string');
		$id = $this->input->get('id', '', 'integer');


	    $jform = $this->input->get("jform", array(), 'array');
	    $post_jform = $this->input->post->get("jform", array(), 'array');

	    //предварительная обработка
	    foreach ($jform as $key=>$item) {
	    	$params = $this->orm->getFormFieldParams($key);

	    	switch ($params['type']) {
				case "fullDate":
					$jform[$key] = $item[0].'-'.$item[1].'-'.$item[2].' '.$item[3].":".$item[4].":".$item[5];
					$post_jform[$key] = $jform[$key];
				break;
			}

	    	if (isset($params['extra'])) {
	    		switch ($params['extra']['update']) {
					case "pseudoname":
						if ($jform[$key] == '') {
							$jform[$key] = CCKHelper::TransUrl($jform[$params['extra']['field']]);
							$post_jform[$key] = $jform[$key];
						}

						if ($jform[$key] == '') {
							return self::error_relay($jform, "Необходимо задать ".$key.".");
						}
					break;
				}
			}
		}

	    $this->input->set("jform", $jform);
	    $this->input->post->set("jform", $post_jform);

	    $return = parent::save($key, $urlVar);
	    if ($return) {
			//постобработка
			$model = $this->getModel();
			$id = $jform['id'] ? $jform['id'] : $model->getMaxID($entity);
			foreach (array_keys($this->orm->fields) as $key) {
				$params = $this->orm->getFormFieldParams($key);
				if (isset($params['extra']))
					switch ($params['extra']['update']) {
						case "current_timestamp":
							$model->update($entity, $key, $id, 'CURRENT_TIMESTAMP()', true);
						break;
					}
			}
		}

		if ($task == 'save')
			$this->setRedirect(JRoute::_('index.php?option=com_cck&view=items&entity='.$entity, false));

		if ($task == 'apply')
			$this->setRedirect(JRoute::_('index.php?option=com_cck&view=item&entity='.$entity."&layout=edit&id=".$id, false));

	    return $return;
	}
}
