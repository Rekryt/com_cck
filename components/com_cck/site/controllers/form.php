<?php defined('_JEXEC') or die;

class CCKControllerForm extends CCKController {
	function display($cachable = false, $urlparams = array()) {
		$helper = new CCKHelper();

		$view = $this->getView('form', 'html');
		$view->setLayout('default');

		if (JRequest::checkToken()) {
			//google reCaptcha v3 token
			$reCaptcha = json_decode(file_get_contents_curl("https://www.google.com/recaptcha/api/siteverify", array(
				'secret'=>$this->params->get("google_recaptcha_secret"),
				'response'=>$this->input->get('gRecaptcha3_token'),
				'remoteip'=>GetIP()
			)));

			if (!$reCaptcha->success) {
				JError::raiseWarning('','Google reCAPTCHAv3 Error');
				return 0;
			}

			$mails = [];
			$data = JRequest::get('post');
			$form = $data['form'];
			if ($this->params->get('email')) $mails[] = $this->params->get('email');
			if ($this->params->get('email2')) $mails[] = $this->params->get('email2');

			foreach (['lechenie','pol','beremennost','drugie_leksredstva','meri','ishod'] as $key) {
				if (!isset($form[$key])) $form[$key] = '';
			}

			foreach ($form as $key=>$value)
				if (is_array($value))
					foreach ($value as $k=>$v)
						if (is_array($v)) {
							$t = [];
							foreach ($v as $item) if ($item) $t[] = $item;
							$form[$key.'|'.$k] = implode('<br>', $t);
						}

			$files = [];
			foreach ($helper->get_input_files() as $file) {
				//if ($file['size'] > 2*1024*1024) $this->ajaxError('Максимальный размер файла 2МБ');

				if (in_array(strtoupper(array_pop(explode(".", $file['name']))), ["EXE", "PHP"]))
					$this->ajaxError('Formats: .exe, .php is deny');

				$uploadfile = tempnam(sys_get_temp_dir(), hash('sha256', $file['name']));
				if (move_uploaded_file($file['tmp_name'], $uploadfile))
					$files[] = [
						'path' => $uploadfile,
						'name' => $file['name']
					];
			}

			if (!$form['fio']) {
				$this->raiseMessage('COM_CCK_NAME_REQUIRED');
			} elseif (sendmail($mails, $this->params->get('notify_subject'), $this->params->get('notify_text'), $form, $files)) {
				$this->raiseMessage('COM_CCK_MAIL_SENT');
			}
		}
		$view->assign('google_recaptcha_key', $this->params->get('google_recaptcha_key'));

		$helper->setMetaData([
			"title"=>$this->params->get('main_title'.langprefix()),
			"description"=>$this->params->get('main_description'.langprefix()),
			"og:title"=>$this->params->get('main_title'.langprefix()),
			"og:description"=>$this->params->get('main_description'.langprefix()),
			"og:url"=>JURI::current(),
			"og:type"=>"website"
		]);

		if ($this->params->get('main_image')) {
			$thumb = generateThumb($this->params->get('main_image'), '800', '');
			$helper->setMetaData([
				"og:image"=>$thumb['thumb_href']
			]);
		}

		parent::display($cachable, $urlparams);
		//JFactory::getApplication()->close();
	}
}
