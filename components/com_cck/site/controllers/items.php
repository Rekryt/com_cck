<?php defined('_JEXEC') or die;

class CCKControllerItems extends CCKController {
	function display($cachable = false, $urlparams = array()) {
		$entity = $this->input->get('entity');
		$active = JFactory::getApplication()->getMenu()->getActive();
		$page = $this->input->get('page', 1, 'integer');

		$view = $this->getView('items', 'html');
		$view->setLayout($active->params->get('layout', $entity));

		$where = [];
		$relation = $active->params->get('predicate') ? $this->getRelation($active->params->get('predicate'), $where) : null;
		//dbg($relation, true);

		$model = $this->getModel('items');
		$pages = ceil($model->get($where, [], true) / $active->params->get('limit'));
		$items = $model->get(
			array_merge($where, [
				'order'=>$active->params->get('order').' '.$active->params->get('dir')
			]),
			[
				($page-1)*$active->params->get('limit'),
				$active->params->get('limit')
			]
		);

		$view->assign('items', $items);
		$view->assign('page', $page);
		$view->assign('pages', $pages);
		$view->assign('params', $active->params);

		parent::display($cachable, $urlparams);
		//JFactory::getApplication()->close();
	}
}
