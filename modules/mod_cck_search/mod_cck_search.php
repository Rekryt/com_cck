<?php
defined('_JEXEC') or die('Restricted access');
require_once(dirname(__FILE__).'/'.'helper.php'); // Подключаем локальный helper
//require_once JPATH_SITE.'/components/com_vp/lib/functions.php';

$lang = JFactory::getLanguage();
$lang->load('com_cck');
$input = JFactory::getApplication()->input;

//$items = modCCKSearchHelper::getList($params);
$search = $input->get('search', '', 'string');

$layout = $params->get('layout', 'default');
// подключаем файл шаблона с помощью класса JModuleHelper
require(JModuleHelper::getLayoutPath('mod_cck_search', $layout));