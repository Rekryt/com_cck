<?php defined('_JEXEC') or die;

class CCKViewCCK extends JViewLegacy {
	protected $items;
	protected $pagination;
	protected $state;

	public function display($tpl = null) {

		$this->state    = $this->get('State');
		$this->items    = $this->get('Items');
		$this->pagination  = $this->get('Pagination');

		$this->params =  JComponentHelper::getParams('com_cck');

		CCKHelper::addSubmenu('cck');

		// Проверить на наличие ошибок.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		//Добавление навигации
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Добавить заголовок страницы и панели инструментов.
	 *
	 * @since  1.6
	 */
	protected function addToolbar()
	{
		//Вывод навигации
		require_once JPATH_COMPONENT.'/helpers/cck.php';

		//Фильтры публикации
		$state  = $this->get('State');
		$canDo  = CCKHelper::getActions($state->get('filter.category_id'));
		$user  = JFactory::getUser();
		// Получить экземпляр объекта панели инструментов
		$bar = JToolBar::getInstance('toolbar');

		//Вывод панели инструментов, добавить, удалить, опубликовать, снять с публикации
		JToolbarHelper::title("AZT-Pharma", 'et.png');
		/*if ($canDo->get('core.create')) {
			JToolbarHelper::addNew('project.add');
		}
		if ($canDo->get('core.edit')) {
			JToolbarHelper::editList('project.edit');
		}

		JToolbarHelper::publish('zg.publish', 'JTOOLBAR_PUBLISH', true);
		JToolbarHelper::unpublish('zg.unpublish', 'JTOOLBAR_UNPUBLISH', true);


		JToolbarHelper::deleteList('', 'zg.delete');
        */
		
		JHtmlSidebar::setAction('index.php?option=com_cck&view=et');
		
		/*JHtmlSidebar::addFilter(
		JText::_('JOPTION_SELECT_PUBLISHED'),
		'filter_state',
		JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.state'), true)
		);*/
	}

	/**
	 * Возвращает массив полей таблицы можно сортировать
	 *
	 * Возвращение массива, содержащий имя поля для сортировки в качестве ключа и текст на дисплее в качестве значения
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
				/*'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
				'a.state' => JText::_('JSTATUS'),
				'a.name' => JText::_('JGLOBAL_TITLE'),
				'a.id' => JText::_('JGRID_HEADING_ID')*/
		);
	}
}