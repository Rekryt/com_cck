<?php
class CCKORModel {
	public $name_table = '';
	public $name_entities = '';
	public $name_entity = '';
	//entity params
	public $params = [
		'items'=>['title'=>'items'],
		'item'=>['title'=>'item']
	];
	//keys of table
	private $keys 		= [];
	//fields of table
	public $fields 	= [];
	private $origin 	= [];
	//list of fields
	public $list		= [];
	//relations of fields
	public $relations	= [];
	//config
	private $config 	= [
		'q'=>'`'
	];


	function __construct() {
		if ($this->name_entities == '') $this->name_entities = $this->name_table;
		if ($this->name_entity == '') $this->name_entity = substr($this->name_table, 0, -1);
	}

	public function key($name, $fields, $type = '') {
		$query = [];
		if ($type) $query[] = $type;
		if ($type == 'PRIMARY')
			$query[] = "KEY (".(is_array($name) ? implode(',', $name) : $this->quotes($name)).")";
		else {
			$query[] = "KEY ".$this->quotes($name);

			$keys = [];
			foreach ($fields as $field) $keys[] = $this->quotes($field);
			$query[] = "(".implode(",", $keys).")";
		}
		$this->keys[] = implode(" ", $query);
	}
	public function addKeys($keys = []) {
		foreach ($keys as $key=>$item) {
			$this->key(
				$key,
				isset($item['fields']) ? $item['fields'] : [],
				isset($item['type']) ? $item['type'] : ''
			);
		}
	}
	public function field($column, $type, $null = '', $default = '', $auto_increment = false, $params = [], $relation = []) {
		$query = [$this->quotes($column)];
		$query[] = $type;
		if ($null != null)
			$query[] = $null;
		if (!$auto_increment && $default != '-')
			$query[] = in_array($default, [
				'NULL',
				'CURRENT_TIMESTAMP',
				'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
			]) ? 'DEFAULT '.$default : "DEFAULT '".$default."'";

		if ($auto_increment)
			$query[] = 'AUTO_INCREMENT';
		if ($type)
			$this->fields[$column] = implode(" ", $query);

		if (isset($this->params['items']['list'][$column])){
			$this->params['items']['list'][$column] = array_merge($this->params['items']['list'][$column], [
				"label"=>isset($params['label']) ? $params['label'] : $column
			]);
			$this->list[$column] = [];
		}

		if ($relation) {
			$in_list = in_array($column, array_keys($this->params['items']['list']));
			$relation['list'] = $in_list;
			$this->relations[$column] = $relation;
		}
	}
	public function addFields($fields = []) {
		foreach ($fields as $key=>$field) {
			$this->field(
				$key,
				isset($field['type']) ? $field['type'] : '',
				isset($field['null']) ? $field['null'] : '',
				isset($field['default']) ? $field['default'] : '',
				isset($field['autoincrement']) ? $field['autoincrement'] : false,
				isset($field['params']) ? $field['params'] : [],
				isset($field['relation']) ? $field['relation'] : []
			);
		}
		$this->origin = array_merge($this->origin, $fields);
	}
	public function create($if_not_exists = false) {
		$tables = [];
		$query = [];
		$query[] = 'CREATE TABLE';
		if ($if_not_exists)
			$query[] = 'IF NOT EXISTS';
		$query[] = $this->quotes('#__cck_'.$this->name_table);

		$fields = [];
		foreach ($this->fields as $item) $fields[] = "    ".$item;
		foreach ($this->keys as $item) $fields[] = "    ".$item;

		$query[] = "(\n".implode(",\n",$fields)."\n)";
		$tables[] = implode(" ", $query);

		foreach ($this->relations as $field=>$relation)
			if (isset($relation['multiple_entity'])) {
				$orm = new CCKORModel();
				$orm->name_table = $relation['multiple_entity'];
				$fields = [];

				$fields[$relation['multiple_entity_field']] = $this->origin[$relation['multiple_key']];
				$fields[$relation['multiple_entity_field']]['autoincrement'] = false;
				$fields[$relation['multiple_entity_field']]['default'] = '-';

				$fields[$relation['multiple_field']] = $this->origin[$relation['multiple_key']];
				$fields[$relation['multiple_field']]['autoincrement'] = false;
				$fields[$relation['multiple_field']]['default'] = '-';

				$orm->key([$relation['multiple_entity_field'], $relation['multiple_field']], [], 'PRIMARY');

				$orm->addFields($fields);
				$tables[] = $orm->create()[0];
			}

		return $tables;
	}
	public function quotes($val) { return $this->config['q'].$val.$this->config['q']; }

	public function getFilters() {
		return $this->params['items']['filters'];
	}
	public function getList() {
		return $this->params['items']['list'];
	}
	public function getForm() {
		return $this->params['item']['form'];
	}
	public function getFormFieldParams($field) {
		return array_merge(isset($this->origin[$field]['params']) ? $this->origin[$field]['params'] : [], ['name'=>$field]);
	}
}