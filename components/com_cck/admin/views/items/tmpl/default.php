<?php defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$user    = JFactory::getUser();
$userId    = $user->get('id');
$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$canOrder  = $user->authorise('core.edit.state', 'com_cck.item');
$saveOrder  = $listOrder == 'a.ordering';
if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_cck&task=items.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'MSEList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();

?>
 
<!--Параметры сортировки-->
<script type="text/javascript">
  Joomla.orderTable = function() {
    table = document.getElementById("sortTable");
    direction = document.getElementById("directionTable");
    order = table.options[table.selectedIndex].value;
    if (order != '<?php echo $listOrder; ?>') {
      dirn = 'asc';
    } else {
      dirn = direction.options[direction.selectedIndex].value;
    }
    Joomla.tableOrdering(order, dirn, '');
  }
</script>


<?php if(!empty($this->sidebar)): ?>
<div id="j-sidebar-container" class="span2">
	<form action="" method="post" enctype="multipart/form-data">
	<?php echo $this->sidebar; ?>
	</form>
</div>
<div id="j-main-container" class="span10">
<?php else : ?>
<div id="j-main-container">
<?php endif;?>
	<?php if(false) : ?>
	<form action="" method="post" enctype="multipart/form-data" style="display: inline-block; padding: 15px; border: 1px solid rgba(0,0,0,0.3)">
		Загрузка xlsx/xls/txt файла
		<table>
			<tr>
				<td>
					<label for="groups_id">Группа:&nbsp;</label>
				</td>
				<td>
					<select name="groups_id" id="groups_id">
						<?php foreach ($this->groups as $item) :?>
							<option value="<?php echo $item->value; ?>"><?php echo $item->text; ?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
		</table>
		<input type="file" name="file">
		<button type="submit">Загрузить</button>
		<input type="hidden" name="option" value="com_cck">
		<input type="hidden" name="view" value="items">
		<input type="hidden" name="task" value="items.import">
	</form>
	<?php endif; ?>
	<form action="<?php echo JRoute::_('index.php?option=com_cck&view=items').'&entity='.$this->entity; ?>" method="post" name="adminForm" id="adminForm">
		<div id="filter-bar" class="btn-toolbar">
			<div class="filter-search btn-group pull-left">
				<label for="filter_<?php echo $this->entity; ?>_search" class="element-invisible">Поиск</label>
				<input type="text" name="filter_<?php echo $this->entity; ?>_search" id="filter_<?php echo $this->entity; ?>_search" placeholder="Поиск"
					   value="<?php echo $this->escape($this->state->get('filter.'.$this->entity.'_search')); ?>"
					   title="<?php echo JText::_('COM_CCK_SEARCH_IN_TITLE'); ?>"/>
			</div>
			<div class="btn-group pull-left">
				<button class="btn hasTooltip" type="submit"
						title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i>
				</button>
				<button class="btn hasTooltip" type="button" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>"
						onclick="document.getElementById('filter_<?php echo $this->entity; ?>_search').value='';this.form.submit();"><i
						class="icon-remove"></i></button>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="limit"
					   class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC');?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="directionTable"
					   class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC');?></label>
				<select name="directionTable" id="directionTable" class="input-medium"
						onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC');?></option>
					<option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING');?></option>
					<option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING');?></option>
				</select>
			</div>
			<div class="btn-group pull-right">
				<label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY');?></label>
				<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JGLOBAL_SORT_BY');?></option>
					<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder);?>
				</select>
			</div>
		</div>
		<div class="clearfix"></div>
		<table class="table table-striped" id="mycomponentList">
			<thead>
			<tr>
				<th width="1%">
					<input type="checkbox" name="checkall-toggle" value=""
						   title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
				</th>
				<?php foreach ($this->fields as $field) : ?>
				<th width="5">
					<?php
					if ($field['sort'])
						echo JHtml::_('grid.sort', JText::_($field['label']), isset($field['sort_field']) ? $field['sort_field'] : $field['field'], $listDirn, $listOrder);
					else
						echo JText::_($field['label']);

					if (isset($field['type']))
						switch ($field['type']) {
							case "text":
								?>
								<a href="javascript:saveorder(<?php echo count($this->items)-1; ?>, 'items.saveorder')" rel="tooltip" class="saveorder btn btn-micro pull-right" title="" data-original-title="Сохранить"><span class="icon-menu-2"></span></a>
								<?php
							break;
						}
					?>
				</th>
				<?php endforeach; ?>
			</tr>
			</thead>

			<tfoot>
			<tr>
				<td colspan="10">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>

			<tbody>
			<!--Запускаем цикл кторый выведет данные на странице-->
			<?php
			foreach ($this->items as $i => $item) :
				$ordering = ($listOrder == 'a.ordering');
				//$canCreate = $user->authorise('core.create', 'com_cck.item.' . $item->catid);
				//$canEdit = $user->authorise('core.edit', 'com_cck.item.' . $item->catid);
				//$canCheckin = $user->authorise('core.manage', 'com_checkin') || $item->checked_out == $user->get('id') || $item->checked_out == 0;
				//$canChange = $user->authorise('core.edit.state', 'com_cck.item.' . $item->catid) && $canCheckin;
			?>
			<tr class="row<?php echo $i % 2; ?>">
				<td>
					<?php echo JHtml::_('grid.id', $i, $item->id); ?>
				</td>
				<?php foreach ($this->fields as $field) : ?>
				<td<?php echo isset($field['width']) ? ' width="'.$field['width'].'"' : '';?>>
					<?php
					if (isset($field['type']) && $field['type']) {
						switch ($field['type']) {
							case "text":
								?>
								<input type="text" name="<?php echo $field['field']; ?>[]" size="5" placeholder="" value="<?php echo $item->{$field['field']};?>" class="text-area-order" style="max-width: 100px; width: calc(100% - 20px);" />
								<?php
							break;
							case "checker":
 								if ($item->{$field['field']} == 1) : ?>
								<a class="btn btn-micro active" href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i; ?>','items.uncheck.<?php echo $field['field']; ?>')" title="Снять" data-original-title="Снять"><i class="icon-publish"></i></a>
								<?php else : ?>
								<a class="btn btn-micro" href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i; ?>','items.check.<?php echo $field['field']; ?>')" title="Установить" data-original-title="Установить"><i class="icon-unpublish"></i></a>
								<?php endif;
							break;
							case "photo":
								if ($item->{$field['field']}) {
									$thumb = generateThumb($item->{$field['field']}, '200', '');
									if(isset($field['link']) && $field['link']) {
										echo '<a href="'.JRoute::_('index.php?option=com_cck&entity='.$this->entity.'&task=item.edit&id=' . (int)$item->id).'"><img src="'.$thumb['thumb_href'].'" alt=""></a>';
									} else {
										echo '<img src="'.$thumb['thumb_href'].'" alt="">';
									}
								}
							break;
						}
					} else {
						if(isset($field['link']) && $field['link']) : ?>
							<a href="<?php echo JRoute::_('index.php?option=com_cck&entity='.$this->entity.'&task=item.edit&id=' . (int)$item->id); ?>"><?php echo $item->{$field['field']}; ?></a>
						<?php else : echo $item->{$field['field']}; endif;
					}
					?>
				</td>
				<?php endforeach; ?>

			</tr>
			<?php endforeach; ?>
			</tbody>

		</table>

		<input type="hidden" name="task" value=""/>
		<input type="hidden" name="boxchecked" value="0"/>
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
		<?php echo JHtml::_('form.token'); ?>
	</form>
</div>
