<?php 
defined('_JEXEC') or die;

class CCKModelCCK extends JModelList {
	//Конструктор
	//Дополнительный ассоциативный массив параметров конфигурации.
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
					'id', 'a.id',
					'name', 'a.name',
					'ordering', 'a.ordering',
					'state', 'a.state'
			);
		}

		parent::__construct($config);
	}

	//Метод для автоматического заполнения model state
	//Примечание. Вызов GetState в этом методе приведет к рекурсии.
	protected function populateState($ordering = null, $direction = null) {
		$app = JFactory::getApplication('administrator');


		// Загрузите состояние фильтра.
		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context.'.filter.state', 'filter_state', '', 'string');
		$this->setState('filter.state', $published);

		// Загрузка параметров
		$params = JComponentHelper::getParams('com_cck');
		$this->setState('params', $params);

		// Список информации о состоянии
		parent::populateState('a.name', 'asc');
	}

	//Родные фильтры joomla с помощью них создаётся поиск и фильтрация опубликованных и неопубликованных данных
	protected function getStoreId($id = ''){
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');
		return parent::getStoreId($id);
	}

	protected function getListQuery(){
		// Создайте новый объект запроса.
		$db    = $this->getDbo();
		$query  = $db->getQuery(true);
		$user  = JFactory::getUser();

		// Выбираем нужные поля из таблицы
		$query->select(1);
		

		return $query;
	}

}