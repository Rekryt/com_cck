<?php defined('_JEXEC') or die;

class CCKControllerCCK extends JControllerAdmin {

	public function getModel($name = 'CCK', $prefix = 'CCKModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	//Метод AJAX что бы сохранить представление записи.
	public function saveOrderAjax() {

		$input = JFactory::getApplication()->input;
		$pks = $input->post->get('cid', array(), 'array');
		$order = $input->post->get('order', array(), 'array');

		JArrayHelper::toInteger($pks);
		JArrayHelper::toInteger($order);

		// Получаем модель
		$model = $this->getModel();

		// Сохранить порядок
		$return = $model->saveorder($pks, $order);

		if ($return)
		{
			echo "1";
		}

		// Закрыть приложение
		JFactory::getApplication()->close();
	}

	public function publish(){
		$input = JFactory::getApplication()->input;
		$pks = $input->post->get('cid', array(), 'array');
		$db = JFactory::getDBO();

		if ($input->get('task') == 'unpublish') {
		  foreach ($pks as $p) {
			  $query = "UPDATE #__mse_reviews SET state = 0 WHERE id = '".$p."'";
			  $db->setQuery($query);
			  $db->execute();
		  }
		} else {
		  foreach ($pks as $p) {
			  $query = "UPDATE #__mse_reviews SET state = 1 WHERE id = '".$p."'";
			  $db->setQuery($query);
			  $db->execute();
		  }
		}

		return parent::publish();
	}

	public function delete() {
		return parent::delete();
	}
}
