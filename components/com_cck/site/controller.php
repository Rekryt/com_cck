<?php defined('_JEXEC') or die;

class CCKController extends JControllerLegacy {
	public $params;
	public $orm;

	public function __construct(array $config = array()) {
		$this->params = JComponentHelper::getParams('com_cck');
		$this->orm = getORM();
		parent::__construct($config);
	}

	function execute($task) {
		$helper = new CCKHelper();
		//$params = JComponentHelper::getParams('com_cck');

		$canonical_url = getRefURL();

		if ($this->input->get("task", "default") == "default") {
			$ref_url = htmlspecialchars_decode($canonical_url);
			$cur_url = parse_url(JURI::current());
			//dbg(array(parse_url($ref_url)['path'], $cur_url['path'], $canonical_altername), true);
			if (parse_url($ref_url)['path'] != $cur_url['path'] && !in_array($cur_url['path'], array("/index.php", "/", "/en/"))) {
				if (substr(JURI::current(), -1, 1) == "/") $helper->redirect($ref_url);
				JError::raiseError( 404, "Страница не найдена" );
				return;
			}
		}

		return parent::execute($task);
	}
	function raiseMessage($message, $type = "message") {
		JFactory::getApplication()->enqueueMessage(JText::_($message), $type);
	}
	function getORMModel($name, $orm) {
		$model = parent::getModel($name);
		$model->orm = $orm;
		return $model;
	}

	public function getRelation($predicate, &$where) {
		$relation = null;

		foreach ($this->orm->relations as $key=>$item)
			if ($item['entity'] == $predicate) { $relation = $item; break; }

		$where[$relation['entity'].".".$relation['field']] = $this->input->get($key);

		$model = $this->getORMModel('items', getORM($predicate));
		return $model->get($where);
	}
}