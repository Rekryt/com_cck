<?php defined('_JEXEC') or die;

class CCKModelItem extends JModelAdmin {
	protected $text_prefix = 'COM_CCK';
	private $orm;

	public function __construct(array $config = array()) {
		//ORM
		$this->orm = getORM();
		parent::__construct($config);
	}

	//Возвращает ссылку на объект таблицы, при его создании.
	public function getTable($type = 'Item', $prefix = 'CCKTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	public function getForm($data = array(), $loadData = true) {
		$db = JFactory::getDBO();

		$fieldsets = [];
		$fieldsets_labels = [];
		$i=1;
		foreach ($this->orm->getForm() as $name=>$list) {
			if(!isset($fieldsets['tab'.$i])) $fieldsets['tab'.$i] = [];
			foreach ($list as $field)
				$fieldsets['tab'.$i][] = $this->orm->getFormFieldParams($field);
				$fieldsets_labels['tab'.$i] = $name;
			$i++;
		}
		// Получить форму
		$xmlData = '<?xml version="1.0" encoding="utf-8"?><form>';
		foreach ($fieldsets as $tabname=>$fieldset) {
			//dbg($fieldset, true);
			$xmlData .= '<fieldset name="'.$tabname.'" label="'.$fieldsets_labels[$tabname].'">';
			foreach ($fieldset as $params) {
				$xmlData .= '<field ';

				if (isset($params['extra'])) {
					$extra = $params['extra'];
					unset($params['extra']);
					//extra
				}

				if (isset($params['source'])) {
					$source = $params['source'];
					unset($params['source']);

					$query = $db->getQuery(true);
					$query
						->select('`'.addslashes($source['field'])."`,`".addslashes($source['value']).'`')
						->from('#__cck_'.addslashes($source['entity']));
					$db->setQuery($query);
					$messages = $db->loadObjectList();

					$options = [];
					foreach($messages as $message)
						$options[$message->{$source['field']}] = $message->{$source['value']};

					$params['options'] = isset($params['options']) ? array_merge($params['options'], $options) : $options;
				}

				if (isset($params['options'])) {
					$options = $params['options'];
					unset($params['options']);

					$xmlData .= implode(' ', array_map(function ($k,$v){ return $k.'="'.htmlspecialchars($v).'"'; }, array_keys($params), $params));
					$xmlData .= '>';

					foreach ($options as $key=>$value) $xmlData .= '<option value="'.$key.'">'.$value.'</option>';

					$xmlData .= '</field>';
				} else {
					$xmlData .= implode(' ', array_map(function ($k,$v){ return $k.'="'.htmlspecialchars($v).'"'; }, array_keys($params), $params));
					$xmlData .= ' />';
				}
			}

			$xmlData .= '</fieldset>';
		}
		$xmlData .= '</form>';

		//$form = $this->loadForm('com_cck.item', 'item', array('control' => 'jform', 'load_data' => $loadData));
		$form = $this->loadForm($xmlData, $xmlData, array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form))
		{
			return false;
		}
		return $form;
	}

	protected function loadFormData() {
		// Проверка сессий для ранее введёных данных формы
		$data = JFactory::getApplication()->getUserState('com_cck.edit.item.data', array());
		if (empty($data))
		{
			$data = $this->getItem();
		}
		return $data;
	}

	public function source($source) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$value = is_array($source['value']) ? $source['value'][0] : $source['value'];
		$query
			->select($source['field'].','.$value)
			->from('#__cck_'.$source['entity']);

		$db->setQuery($query);
		$messages = $db->loadObjectList();

		$options = array();
		if ($messages)
			foreach($messages as $message)
				$options[] = JHtml::_('select.option', $message->{$source['field']}, $message->{$value});

		return $options;
	}

	public function getMultipleData($source, $id) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		return [];
	}

	function update($entity, $field, $id, $value, $raw_value = false) {
		$db = $this->getDbo();

		$query = "
			UPDATE `#__cck_".$entity."`
			SET `".$field."` = ".($raw_value ? $value : "'".addslashes($value)."'")."
			WHERE id = '".addslashes($id)."'";
		$db->setQuery($query);
		$res = $db->query();

		return true;
	}

	function getMaxID($entity) {
		$db = $this->getDbo();

		$query = "SELECT MAX(id) as max FROM `#__cck_".$entity."`";
		$db->setQuery($query);

		return $db->loadObjectList()[0]->max;
	}

}