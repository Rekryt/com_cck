<?php defined('_JEXEC') or die('Restricted access');
/**
 * Class modCCKMenuHelper
 */
class modCCKMenuHelper {
    function getItems($filter=array()) {
        $lang = JFactory::getLanguage();
		$menu = JFactory::getApplication()->getMenu();
		$active = $menu->getActive();

        //defaults
        foreach (array("m.published"=>1, "lang"=>$lang->getTag(), "notalias"=>false) as $key=>$val) if (!isset($filter[$key])) $filter[$key] = $val;
        
        $db = JFactory::getDBO();
        $query  = $db->getQuery(true);
        
        //filters
        foreach ($filter as $key=>$val) {
            switch ($key) {
                case "lang":
                    $query->where("m.language IN ('*', '".addslashes($val)."')");
                break;
                case "notalias":
                    if ($val) $query->where("m.type != 'alias'");
                break;
                case "order":break;
                default:
                    $query->where($key." = '".addslashes($val)."'");
                break;
            }
        }
        
        $query
        ->select("m.*")
        ->from("#__menu m")
        ->leftjoin("#__menu_types t ON t.menutype = m.menutype");
        
        if (isset($filter['order'])) {
            $query->order($filter['order']);
        } else {
            $query->order("t.id DESC, m.lft");
        }
        //dbg((string) $query, true);
        $db->setQuery($query);
        $items = $db->loadObjectlist();
        
        foreach ($items as $i=>$item) {
            $params = json_decode($item->params);
            $item->flink = $item->link;
            $item->active = $item->id == $active->id;

            switch ($item->type){
                case 'separator': break; 
                case 'heading': break;
                case 'url':
                    $item->href = $item->link;
                break;
                case 'alias':
                    $item->href = JRoute::_('index.php?Itemid=' . $params->aliasoptions);
                break;
                default:
                    $item->href = JRoute::_('index.php?Itemid=' . $item->id);
                break;
            }
            
            
            
//             if ($item->type == "url") {
//                 $item->href = $item->link;
//             } else {
//                 $item->href = getSEFlink($item->link);
//             }
        }
        
        $rows = array();
        foreach ($items as $row) $rows[$row->parent_id][] = $row;
        
        return $rows;
    }
}

