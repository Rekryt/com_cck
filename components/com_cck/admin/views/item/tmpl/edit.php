<?php

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

//$doc = JFactory::getDocument();
//$doc->addScript("/templates/qadviser/js/jquery-2.1.3.min.js");
//$doc->addScript("/administrator/components/com_qadviser/js/script.js");

?>
<!--Сохранение данных из поля редактора, если этого не поствить то ничего работать не будет
echo $this->form->getField('opisanie')->save();
где opisanie поле в бд
-->
<script type="text/javascript">
  Joomla.submitbutton = function(task)
  {
    if (task == 'item.cancel' || document.formvalidator.isValid(document.id('mycomponent-form'))) {
      <?php //echo $this->form->getField('opisanie')->save(); ?>
      Joomla.submitform(task, document.getElementById('mycomponent-form'));
    }
    else {
      alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
    }
  }


</script>

<form action="<?php echo JRoute::_('index.php?option=com_cck&layout=edit&id='.(int) $this->item->id).'&entity='.$this->entity; ?>" method="post" name="adminForm" id="mycomponent-form" class="form-validate">
<div class="fltlft">
<fieldset class="adminform">

<div class="form-horizontal">
<ul id="myTabTabs" class="nav nav-tabs">
	<?php $i=0; foreach ($this->form->getFieldsets() as $key=>$fieldset) : $i++; ?>
	<li<?php echo $i==1 ? ' class="active"' : ''; ?>>
		<a href="#<?php echo $fieldset->name; ?>" data-toggle="tab"><?php echo $fieldset->label; ?></a>
	</li>
	<?php endforeach; ?>
</ul>
<div id="myTabContent" class="tab-content">
	<?php $i=0; foreach ($this->form->getFieldsets() as $key=>$fieldset) : $i++; ?>
	<div id="<?php echo $fieldset->name; ?>" class="tab-pane<?php echo $i==1 ? ' active' : ''; ?>">
		<?php
		foreach($this->form->getFieldset($key) as $field) {
			$name = $field->getAttribute('name');
			switch ($field->getAttribute('type')) {
				case "multipleSelect":
					//echo '<label for="jform[lots_id][]" class="">Лоты</label>';
					if (!$field->hidden) echo $field->label;
					echo JHTML::_('select.genericlist', $this->relation[$name], "jform[".$name."][]", array("multiple"=>"true"), 'value', 'text', $this->relation_data[$name], false, false);
					echo "<br><br>";
				break;
				case "select":
					//echo '<label for="jform[groups_id]" class="">Группа</label>';
					if (!$field->hidden) echo $field->label;
					echo JHTML::_('select.genericlist', $this->relation[$name], $field->name, null, 'value', 'text', $field->value, false, false);
					echo "<br><br>";
				break;
				case "fullDate":
					echo $field->label;
					?>
					<input type="text" name="jform[<?php echo $name; ?>][]" placeholder="год" value="<?php echo JHtml::_('date', strtotime($field->value), 'Y'); ?>" size="20" aria-invalid="false" style="width: 30px" maxlength="4">-
					<input type="text" name="jform[<?php echo $name; ?>][]" placeholder="месяц" value="<?php echo JHtml::_('date', strtotime($field->value), 'm'); ?>" size="20" aria-invalid="false" style="width: 15px" maxlength="2">-
					<input type="text" name="jform[<?php echo $name; ?>][]" placeholder="число" value="<?php echo JHtml::_('date', strtotime($field->value), 'd'); ?>" size="20" aria-invalid="false" style="width: 15px" maxlength="2">&nbsp;
					<input type="text" name="jform[<?php echo $name; ?>][]" placeholder="час" value="<?php echo JHtml::_('date', strtotime($field->value), 'H'); ?>" size="20" aria-invalid="false" style="width: 15px" maxlength="2">:
					<input type="text" name="jform[<?php echo $name; ?>][]" placeholder="мин" value="<?php echo JHtml::_('date', strtotime($field->value), 'i'); ?>" size="20" aria-invalid="false" style="width: 15px" maxlength="2">:
					<input type="text" name="jform[<?php echo $name; ?>][]" placeholder="сек" value="<?php echo JHtml::_('date', strtotime($field->value), 's'); ?>" size="20" aria-invalid="false" style="width: 15px" maxlength="2">
					<br><br>
					<?php
				break;
				case "raw":
					if ($field->value) {
						if (!$field->hidden) echo $field->label;
						echo "<p>".$field->value;
					}
				break;
				case "media":
				case "mediajce":
					echo $field->label;
					if ($field->value) {
						$thumb = generateThumb($field->value, '200', '');
						echo '<img src="'.$thumb['thumb_href'].'" alt="">';
					}
					echo $field->input."<div style='clear: both'></div><br>";
				break;
				default:
					if (!$field->hidden) echo $field->label;
					echo $field->input;
					if (!$field->hidden) echo "<div style='clear: both'></div><br>";
				break;
			}
		}
		?>
	</div>
	<?php endforeach; ?>
</div>
</div>


</fieldset>
</div>

<div>
	<input type="hidden" name="jform[fakeField]" value="" />
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</div>
</form>
