<?php defined('_JEXEC') or die;

class CCKModelItems extends CCKJModelLegacy {
	public $_context = 'com_cck.items';

	function get($filter=array(), $limit = array(), $count = false) {
		$store = $this->getStoreId(array('get', $filter, $limit, $count));
		if (!($rows = $this->cache($store))) {
			$db = $this->getDbo();
			$query  = $db->getQuery(true);
			$alias = $this->orm->name_entities;
			//defaults
			foreach (array("$alias."."state".langprefix()=>1) as $key=>$val) if (!isset($filter[$key])) $filter[$key] = $val;

			// Выбираем нужные поля из таблицы
			if ($count) {
				$query->select("COUNT(*) as count");
			} elseif (isset($filter['fields']) && is_array($filter['fields'])) {
				//foreach (array_keys($this->orm->list) as $field) $query->select("$alias.".$field);
				foreach ($filter['fields'] as $field) $query->select("$alias.".$field);
			} else {
				$query->select("$alias.*");
			}

			$query->from('#__cck_'.$this->orm->name_table." AS $alias");

			//relations
			foreach ($this->orm->relations as $key=>$relation) {
				if (!$relation['list']) continue;
				$p = $relation['entity'];
				if (isset($relation['multiple_entity'])) {
					$query->leftJoin('#__cck_'.$relation['multiple_entity']." rel_$p ON rel_$p.".$relation['multiple_entity_field']." = a.".$relation['multiple_key']);
					$query->leftJoin('#__cck_'.$relation['entity']." $p ON $p.".$relation['field']." = rel_$p.".$relation['multiple_field']);
				} else {
					$query->leftJoin('#__cck_'.$relation['entity']." $p ON $p.".$relation['field']." = ".$alias.".".$key);
				}
				if (!$count)
					foreach ($relation['value'] as $item)
						$query->select("$p.".$item." as ".$relation['entity']."_".$item);
			}

			//filters
			foreach ($filter as $key=>$val) {
				switch ($key) {
					case "order":
						$query->order(addslashes($val));
					break;
					default:
						$query->where($key." = '".addslashes($val)."'");
					break;
				}
			}

			#limits
			if(count($limit) == 1) $query->setLimit($limit[0]); elseif (count($limit) == 2) $query->setLimit($limit[1], $limit[0]);

			$query->order("$alias."."id");

			//if (!$count) dbg((string) $query, true);
			$db->setQuery($query);
			$rows = langfix($db->loadObjectlist());
			if ($count) {
				$rows = $rows[0]->count;
			} else {
				foreach ($rows as $row) {
					if (isset($relation)) {
						$row->href = getSEFlink(
							'index.php?option=com_cck&view=items&entity='.$alias."&".$relation['entity']."_".$relation['field']."=".$row->{$relation['entity']."_".$relation['field']},
							['view'=>'item', 'id'=>$row->id]
						);
					} else {
						$row->href = getSEFlink(
							'index.php?option=com_cck&view=items&entity='.$alias,
							['view'=>'item', 'id'=>$row->id]
						);
					}

				}
			}

			$this->store($rows, $store);
		}
		return $rows;
	}

	function search($entities, $filter_init=array(), $limit = array(), $count = false) {
		$db = $this->getDbo();

		$store = $this->getStoreId(array('search', $entities, $filter_init, $limit, $count));
		if (!($rows = $this->cache($store))) {
			$union = [];
			foreach ($entities as $entity) {
				$filter = $filter_init;
				$query  = $db->getQuery(true);

				$this->orm = getORM($entity);
				$alias = $this->orm->name_entities;
				//defaults
				foreach (array(
					"$alias."."state".langprefix()=>1,
					 'fields'=>['id', 'name_ru', 'name_en','date_create'],
					 'searchFields'=>['name'.langprefix(), 'text'.langprefix(), 'desc'.langprefix()]
				 ) as $key=>$val) if (!isset($filter[$key])) $filter[$key] = $val;

				// Выбираем нужные поля из таблицы
				foreach ($filter['fields'] as $field) $query->select("$alias.".$field);

				$query->from('#__cck_'.$this->orm->name_table." AS $alias");

				//relations
				$query->select("'".$alias."' as sys_alias");
				if ($this->orm->relations) {
					foreach ($this->orm->relations as $key=>$relation) {
						if (!$relation['list']) continue;
						$p = $relation['entity'];
						if (isset($relation['multiple_entity'])) {
							$query->leftJoin('#__cck_'.$relation['multiple_entity']." rel_$p ON rel_$p.".$relation['multiple_entity_field']." = a.".$relation['multiple_key']);
							$query->leftJoin('#__cck_'.$relation['entity']." $p ON $p.".$relation['field']." = rel_$p.".$relation['multiple_field']);
						} else {
							$query->leftJoin('#__cck_'.$relation['entity']." $p ON $p.".$relation['field']." = ".$alias.".".$key);
						}
						break;
					}
				}
				$query->select("'".($this->orm->relations ? $relation['entity'] : '')."' as rel_entity");
				$query->select("'".($this->orm->relations ? $relation['field']: '')."' as rel_field");
				$query->select(($this->orm->relations ? $relation['entity'].".".$relation['field'] : "''")." as rel_value");


				foreach ($filter as $key=>$val) {
					switch ($key) {
						case "order":
						case "fields":
						case "searchFields":
						break;
						case "search":
							$search = [];
							foreach ($filter['searchFields'] as $field)
								if (in_array($field, array_keys($this->orm->fields)))
									$search[] = $alias.'.'.$field." like '%".addslashes($val)."%'";
							if ($search) $query->where('('.implode(' OR ', $search).')');
						break;
						default:
							$query->where($key." = '".addslashes($val)."'");
						break;
					}
				}

				$union[] = (string) $query;
			}

			$query  = $db->getQuery(true);
			if ($count) {
				$query->select("COUNT(*) as count");
			} else {
				$query->select('T.*');
			}
			$query->from('(('.implode(') UNION (', $union).')) T');

			foreach ($filter_init as $key=>$val) {
				switch ($key) {
					case "order":
						$query->order(addslashes($val));
					break;
				}
			}

			#limits
			if(count($limit) == 1) $query->setLimit($limit[0]); elseif (count($limit) == 2) $query->setLimit($limit[1], $limit[0]);
			//dbg((string)$query, true);

			$db->setQuery($query);
			$rows = langfix($db->loadObjectlist());

			if (!$count)
				foreach ($rows as $row)
					if ($row->rel_entity) {
						$row->href = getSEFlink(
							'index.php?option=com_cck&view=items&entity='.$row->sys_alias."&".$row->rel_entity."_".$row->rel_field."=".$row->rel_value,
							['view'=>'item', 'id'=>$row->id]
						);
					} else {
						if (in_array($row->sys_alias, ['pages'])) {
							$row->href = getSEFlink(
								'index.php?option=com_cck&view=item&entity='.$row->sys_alias.'&'.$row->sys_alias."_id=".$row->id
							);
						} else {
							$row->href = getSEFlink(
								'index.php?option=com_cck&view=items&entity='.$row->sys_alias,
								['view'=>'item', 'id'=>$row->id]
							);
						}
					}


			if ($count) $rows = $rows[0]->count;
			$this->store($rows, $store);
		}
		return $rows;
	}
}