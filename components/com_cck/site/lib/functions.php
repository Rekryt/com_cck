<?php

function getRefURL($query_update=array()) {

	if (isset($query_update['lang'])) {
		$menu = JFactory::getApplication()->getMenu();
		$active = $menu->getActive();
		JLoader::register('MenusHelper', JPATH_ADMINISTRATOR . '/components/com_menus/helpers/menus.php');
		$associations = MenusHelper::getAssociations($active->id);
		if ($associations[$query_update['lang']]) $query_update['Itemid'] = $associations[$query_update['lang']];
	}

	$input = JFactory::getApplication()->input;
	$lang = JFactory::getLanguage();

	$pageState = $input->get('pageState', array(), 'array');
	unset($pageState['href']);
	foreach ($pageState as $key=>$val) $input->set($key, $val);

	$vars = array(
		"id", "Itemid", "option", "view", "task", "entity", "lang",
		"categories_id", "pages_id", "search"
	);
	$vars = array_merge($vars, array_keys($pageState));
	//$vars = array("Itemid");

	$query = array();
	if (!(isset($query_update['lang']))) $query_update['lang'] = $lang->getTag();
	foreach ($vars as $v) {
		if ($input->get($v)) if (in_array($v, array('search'))) {
			$query[$v] = $input->get($v, '', 'string');
		} else {
			$query[$v] = $input->get($v);
		}
	}

	foreach ($query_update as $k=>$u) {
		$query[$k] = $u;
	}

	if ( !(isset($query['id']) && $query['id']) ) { unset($query['id']); }

	foreach ($query as $k=>$v) {
		if (!in_array($k, $vars)) {
			unset($query[$k]);
		}
	}
	$q = explode("&",JURI::getInstance()->buildQuery($query));
	//dbg($q, true);
	return JRoute::_(("index.php?".implode("&", $q)));
}

function getSEFlink($link = "index.php?option=com_cck&view=cck", $params = array(), $get_itemid = true) {
	global $links;
	$app = JFactory::getApplication();
	$db =  JFactory::getDBO();
	$lang = JFactory::getLanguage();
	$lang_itemId = array(
		"ru-RU"=>"104",
		"en-GB"=>"102"
	);

	if ($get_itemid) {
		$lang_tag = isset($params['lang']) ? $params['lang'] : $lang->getTag();
		if (isset($links[$link])) { $itemid = $links[$link]; } else {
			$query = "SELECT *
               FROM `#__menu`
               WHERE `link` LIKE '%".addslashes($link)."%'
               AND `published` = 1 AND (language = '*' OR language = '".$lang_tag."')
               LIMIT 1";

			$db->setQuery($query);
			$itemid=$db->loadObjectlist();
			$links[$link] = $itemid;
		}

		$parts = array();
		foreach ($params as $key=>$val) $parts[] = $key.'='.$val;
		return JRoute::_($link.(count($parts)>0 ? "&".implode('&', $parts) : '').'&Itemid='.(isset($itemid[0]->id) ? $itemid[0]->id : $lang_itemId[$lang->getTag()]));
	} else {
		$active = $app->getMenu()->getActive();
		return JRoute::_($link.'&Itemid='.($active->id ? $active->id : $lang_itemId[$lang->getTag()]));
	}
}

function JModulePosition( $position, $style=-2 ) {
	$document    = &JFactory::getDocument();
	$renderer    = $document->loadRenderer('module');
	$params        = array('style'=>$style);

	$contents = '';

	foreach (JModuleHelper::getModules($position) as $mod)
	{
		$contents .= $renderer->render($mod, $params);
	}

	return $contents;
}

function D_Error_List(){
	$app = JFactory::getApplication();
	$messageQueue = $app->getMessageQueue();
	$ex = "";
	foreach ($messageQueue as $mq) {
		switch ($mq['type']) {
			case "message":
			default:
				$ex .= "<p>".$mq['message']."</p>";
				break;
		}
	}
	return $ex;
}

function langprefix($reverse = false) {
	$lang = JFactory::getLanguage();
	switch ($lang->getTag()) {
		case "en-GB": $lang_prefix = "_en"; break;
		case "ru-RU":
		default: $lang_prefix = "_ru"; break;
	}
	if ($reverse && $lang_prefix == "_ru") return "_en";
	if ($reverse && $lang_prefix == "_en") return "_ru";
	return $lang_prefix;
}

function inclean($count, $array=array()){
	if ($count == 0) {
		return $array[2];
	} elseif ($count == 1) {
		return $array[0]; //штука
	} elseif ($count > 1 && $count < 5) {
		return $array[1]; //штуки
	} elseif ($count>=5 && $count<21) {
	    return $array[2]; //штук
	} else {
	    return inclean(substr($count, -1), $array);
	}
}

function dbg($var, $exit = false) {
	echo "<pre>".print_r($var, true)."</pre>";
	if ($exit) exit();
}

function gettime($time = 0){
    //date_default_timezone_set("Etc/GMT-3");
    date_default_timezone_set("UTC");
    //return time();
    if ($time == 0) {
        return time()+gettime_offset();
    } else {
        return $time+gettime_offset();
    }
}

function gettime_offset() {
    return (-1)*60*60;
}

function tz_offset() {
    return 4*60*60;
}

function formatted_date($date) {
	$months = array(
		1 => JText::_('COM_CCK_MONTHS_1'),
		2 => JText::_('COM_CCK_MONTHS_2'),
		3 => JText::_('COM_CCK_MONTHS_3'),
		4 => JText::_('COM_CCK_MONTHS_4'),
		5 => JText::_('COM_CCK_MONTHS_5'),
		6 => JText::_('COM_CCK_MONTHS_6'),
		7 => JText::_('COM_CCK_MONTHS_7'),
		8 => JText::_('COM_CCK_MONTHS_8'),
		9 => JText::_('COM_CCK_MONTHS_9'),
		10 => JText::_('COM_CCK_MONTHS_10'),
		11 => JText::_('COM_CCK_MONTHS_11'),
		12 => JText::_('COM_CCK_MONTHS_12')
	);

	$date = strtotime($date);
	$date_d = date("d", $date);
	$date_m = $months[date("n", $date)];
	$date_y = date("Y", $date);

	if ( date("Y", time()) !=  $date_y) {
		$date = $date_d." ".$date_m.", ".$date_y;
	} else {
		$date = $date_d." ".$date_m;
	}

	return $date;

}

function vc_filename ($string) {
	$mustchar = "ЁЙЦУКЕНГШЩЗХЪЭЖДЛОРПАВЫФЯЧСМИТЬБЮёйцукенгшщзхъэждлорпавыфячсмитьбюQWERTYUIOPLKJHGFDSAZXCVBNMqwertyuioplkjhgfdsazxcvbnm.,-_1234567890() ";
	for ($i=1; $i<=strlen($string); $i++) {
		if ( strpos($mustchar, $string[$i-1]) === false ) { return "#"; $k = 1; }
	}
	return $string;
}

function delTree($dir) {
	if (is_dir($dir)) {
		$files = array_diff(scandir($dir), array('.','..'));
		foreach ($files as $file) {
			(is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
		}
		return rmdir($dir);
	} else {
		return 1;
	}
	
}

function convert_time($time) {
	$time = explode(":", $time);
	$time = $time[0]*60+$time[1];
	return $time;
}

function reconvert_time($time) {
	$h = floor($time / 60);
	$m = $time - $h*60;
	if (strlen($h) == 1) $h = "0".$h;
	if (strlen($m) == 1) $m = "0".$m;
	
	return $h.":".$m;
}

function TransUrl($str) {
	$tr = array(
		"А"=>"a","Б"=>"b","В"=>"v","Г"=>"g","Д"=>"d","Е"=>"e","Ё"=>"yo","Ж"=>"zh","З"=>"z","И"=>"i","Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n","О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t","У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch","Ш"=>"sh","Щ"=>"shch","Ъ"=>"","Ы"=>"y","Ь"=>"","Э"=>"e","Ю"=>"yu","Я"=>"ya",
		"а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"zh","з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l","м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r","с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h","ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"shch",	"ъ"=>"","ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
		" "=>"-","-"=>"-",
		"A"=>"a","B"=>"b","C"=>"c","D"=>"d","E"=>"e","F"=>"f","G"=>"g","H"=>"h","I"=>"i","J"=>"j","K"=>"k","L"=>"l","M"=>"m","N"=>"n","O"=>"o","P"=>"p","Q"=>"q","R"=>"r","S"=>"s","T"=>"t","U"=>"u","V"=>"v","W"=>"w","X"=>"x","Y"=>"y","Z"=>"z",
		"a"=>"a","b"=>"b","c"=>"c","d"=>"d","e"=>"e","f"=>"f","g"=>"g","h"=>"h","i"=>"i","j"=>"j","k"=>"k","l"=>"l","m"=>"m","n"=>"n","o"=>"o","p"=>"p",
		"q"=>"q","r"=>"r","s"=>"s","t"=>"t","u"=>"u","v"=>"v","w"=>"w","x"=>"x","y"=>"y","z"=>"z",
		"0"=>"0","1"=>"1","2"=>"2","3"=>"3","4"=>"4","5"=>"5","6"=>"6","7"=>"7","8"=>"8","9"=>"9",
		"."=> "","/"=> "",","=>"","("=>"",")"=>"","["=>"","]"=>"","="=>"","+"=>"",
		"*"=>"","?"=>"","\""=>"","'"=>"","&"=>"","%"=>"","#"=>"","@"=>"","!"=>"",";"=>"","№"=>"n",
		"^"=>"",":"=>"","~"=>"","\\"=>"", "`"=>"", ":"=>"", "«"=>"", "»"=>"", "—"=>"-", "’"=>""
	);
	return str_replace("--", "-", str_replace("--", "-", str_replace("---", "-", strtr(strtolower($str),$tr))));
}

function getYoutubeID( $url ){

	if( !strpos($url, "#!v=") === false ){  //Em caso de ser um link de quando clica nos related
		//In case of be a link from related
		$url = str_replace('#!v=','?v=',$url);
	}

	parse_str( parse_url( $url, PHP_URL_QUERY ) );

	if( isset( $v ) ){
		return $v;
	} else { //Se não achou, é por que é o link de um video de canal ex: http://www.youtube.com/user/laryssap#p/a/u/1/SAXVMaLL94g
		//If not found, is because is a link from a user channel ex: http://www.youtube.com/user/laryssap#p/a/u/1/SAXVMaLL94g
		return substr( $url, strrpos( $url,'/') + 1, 11);
	}
}

function bot_detection(){
	$bot = 'Unknown';
	
	if (stristr($_SERVER['HTTP_USER_AGENT'], 'Yandex')){ $bot='Yandex';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'YandexBot')){ $bot='YandexBot';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'Yandex')){ $bot='Yandex';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'YandexDirect')){ $bot='Yandex Direct';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'Googlebot')){$bot='Googlebot';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'Google')){$bot='Google';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'Mediapartners-Google')){$bot='Mediapartners-Google (Adsense)';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'Slurp')){$bot='Hot&nbsp;Bot&nbsp;search';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'WebCrawler')){$bot='WebCrawler&nbsp;search';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'ZyBorg')){$bot='Wisenut&nbsp;search';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'scooter')){$bot='AltaVista';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'StackRambler')){$bot='Rambler';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'Aport')){$bot='Aport';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'lycos')){$bot='Lycos';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'Yahoo')){$bot='Yahoo';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'WebAlta')){$bot='WebAlta';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'yahoo')){$bot='Yahoo';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'msnbot')){$bot='msnbot/1.0';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'ia_archiver')){$bot='Alexa search engine';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'FAST')){$bot='AllTheWeb';}
	else if (stristr($_SERVER['HTTP_USER_AGENT'], 'SputnikBot')){$bot='Sputnik';}
	
	return $bot;
}

function array_trim($arr) {
    $ex = array();
    foreach ($arr as $k=>$a) {
        if (strlen(trim($a))>0) $ex[$k] = trim($a);
    }
    return $ex;
}

function patterns($array, $pattern, $keys){
	foreach ($array as $key=>$val) {
		foreach ($val as $k=>$v)
			if (in_array($k, $keys)) {
				foreach ($pattern as $pk=>$pv) {
					$array[$key]->{$k} = str_replace("{".$pk."}", $pv, $array[$key]->{$k});
				}
			}
	}
	return $array;
}


function GetIP() {
	if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
		$ip = getenv("HTTP_CLIENT_IP");
	else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
		$ip = getenv("HTTP_X_FORWARDED_FOR");
	else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
		$ip = getenv("REMOTE_ADDR");
	else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
		$ip = $_SERVER['REMOTE_ADDR'];
	else
		$ip = "unknown";
	return $ip;

} // GetIP

function file_get_contents_curl($url, $post = array(), $mediatype = 'multipart/form-data') {
    $ch = curl_init();
    
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Устанавливаем параметр, чтобы curl возвращал данные, вместо того, чтобы выводить их в браузер.
    
    switch ($mediatype) {
        case "application/json":
            $data_string = json_encode($post);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
                );
            break;
        default: //multipart/form-data
            curl_setopt($ch, CURLOPT_HEADER, 0);
            if (count($post) > 0) {
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
            }
            break;
    }
    
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    //curl_setopt($ch, CURLOPT_LOW_SPEED_LIMIT, 1);
    //curl_setopt($ch, CURLOPT_LOW_SPEED_TIME, 1);
    curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
    
    $data = curl_exec($ch);
    curl_close($ch);
    
    return $data;
}

function sendmail($emails, $subject, $text, $data = array(), $files = array(), $bcc = array()) {
	$config = JFactory::getConfig();

	$data['fromname'] = $config->get('fromname');
	$data['mailfrom'] = $config->get('mailfrom');
	$data['sitename'] = $config->get('sitename');
	$data['siteurl'] = JUri::base();

	$pattern = array();
	foreach ($data as $key=>$val) {
		$pattern["{".$key."}"] = $val;
	}

	#Отправка почты
	$emailSubject = strtr($subject, $pattern);
	$emailBody = strtr($text, $pattern);

	$mailer = JFactory::getMailer();
	$mailer->setSender(array($data['mailfrom'], $data['fromname']));

	foreach ($emails as $email) { $mailer->addRecipient($email); }
	foreach ($bcc as $email) { $mailer->addBCC($email); }

	//files
	foreach ($files as $file) $mailer->AddAttachment($file['path'], $file['name']);//, $file['encoding'], $file['type']);

	$mailer->setSubject($emailSubject);
	$mailer->setBody($emailBody);

	$mailer->isHTML(true);
	return $mailer->Send();
}

function string_patterns($string, $pattern = array()){
	foreach ($pattern as $pk=>$pv) $string = str_replace("{".$pk."}", $pv, $string);
	return $string;
}

function imageSize($file, $getpath = true) {
    if ($getpath) {
        if (!(substr($file, 0, 1) == "/" || substr($file, 0, 1) == "\\")) {
            $file = "/".$file;
        }
        $file = iconv('UTF-8', 'windows-1251', $file);
        
        $path = JPATH_ROOT.$file;
    } else {
        $path = $file;
    }
    
    
    if ( !(@fopen($path, "r")) ) {
        echo $path." - фото не найдено";
        return false;
    }
    
    $size = getimagesize($path);
    return array($size[0], $size[1]);
}

function generateThumb ($file, $width, $height, $cacheSubdir="thumbs/",$action="crop",$params=array()) {
    
    if (!(substr($file, 0, 1) == "/" || substr($file, 0, 1) == "\\")) {
        $file = "/".$file;
    }
    
    $cache_dir = "/img/";
    
    if ( !is_dir(JPATH_ROOT.$cache_dir.$cacheSubdir) ) {
        mkdir(JPATH_ROOT.$cache_dir.$cacheSubdir, 0755);
    }
    //$file = iconv('UTF-8', 'windows-1251', $file);
    
    $dir = explode('/',$file);
    $file = array_pop($dir); $res = '';
    for ($i=1;$i<count($dir);$i++) {
        $res .= '/'.$dir[$i];
    }
    $dir = $res."/";
    
    $file_parts = explode('.',$file);
    $ext = strtolower(array_pop($file_parts));
    if ($ext!="png") {
        $ext = "jpg";
    }
    
    if ($action == "png") {
        $ext = "png";
    }
    
    
    $file_name = implode('.',$file_parts);
    $file_size = filesize(JPATH_ROOT.$dir.$file);
    //dbg(array($file, $file_size), true);
    
    $thumb_name = md5($dir."-".$file_name."-".$action."-".$file_size."-".serialize($params))."-".$width."x".$height.".".$ext;
    
    $src_title = urldecode(htmlspecialchars($file_name));
    $thumb_title = urldecode(htmlspecialchars($thumb_name));
    
    
    /*$log_ex = time()." ".$src_title." ".$thumb_title." ".JPATH_ROOT.$cache_dir.$cacheSubdir.$thumb_name." ".(@fopen(JPATH_ROOT.$cache_dir.$cacheSubdir.$thumb_name, "r"))."\n";
     $log = fopen(JPATH_ROOT.$cache_dir."thumbs.log", "a");
     fputs($log, $log_ex, strlen($log_ex));
     fclose($log);*/
    
    //создание thumb-а
    //if ( !is_file(JPATH_ROOT.$cache_dir.$cacheSubdir.$thumb_name) ) {
    
    if ( !(@fopen(JPATH_ROOT.$cache_dir.$cacheSubdir.$thumb_name, "r")) ) {
        switch ($action) {
            case "png":
                $ex = pngImage(JPATH_ROOT.$dir.$file, JPATH_ROOT.$cache_dir.$cacheSubdir.$thumb_name, $width, $height, $params);
                break;
            case "crop":
            default:
                $ex = cropImage(JPATH_ROOT.$dir.$file, JPATH_ROOT.$cache_dir.$cacheSubdir.$thumb_name, $width, $height, $params);
                break;
        }
    }
    
    $thumbs['name'] = $file;
    $thumbs['src_href'] = $dir.$file;
    $thumbs['thumb_href'] = $cache_dir.$cacheSubdir.$thumb_title;
    return $thumbs;
}

function cropImage($path, $npath, $width, $height, $params = array()) {
    if ( !(@fopen($path, "r")) ) {
        //echo $path." - фото не найдено";
        return false;
    }
    switch ( strtolower(strrchr($path, '.')) ){
        case ".jpg":
        case ".jpeg":
            $srcImage = @ImageCreateFromJPEG($path);
            break;
        case ".gif":
            $srcImage = @ImageCreateFromGIF($path);
            break;
        case ".png":
            $srcImage = @ImageCreateFromPNG($path);
            break;
    }
    
    $srcWidth = ImageSX($srcImage);
    $srcHeight = ImageSY($srcImage);
    
    if ($height && $srcHeight < $height ) {
        $height = $srcHeight;
    }
    if ($width && $srcWidth < $width) {
        $width = $srcWidth;
    }
    
    $h0 = ($srcHeight/$srcWidth)*$width;
    $w0 = ($srcWidth/$srcHeight)*$height;
    
    if ( !($height) ) $height = $h0;
    if ( !($width) ) $width = $w0;
    
    $resImage = ImageCreateTrueColor($width, $height);
    imagefill($resImage, 0, 0, 0xffffff);
    
    $wd = ($srcWidth-$width)/2; //if ($wd<2) $wd=0;
    $hd = ($srcHeight-$height)/2; //if ($hd<2) $hd=0;
    
    if ( $srcWidth < $width && $srcHeight < $height ) {
        imagecopyresampled($resImage, $srcImage, (-1)*$wd, (-1)*$hd, 0, 0, $srcWidth, $srcHeight, $srcWidth, $srcHeight);
    } else {
        if ( $h0 >= $height ) { //обрезаем по высоте
            $srcHd =  0.5*($h0 - $height)*($srcHeight/$h0);
            $srcH = $srcHeight - 2*$srcHd;
            
            imagecopyresampled($resImage, $srcImage, 0, 0, 0, $srcHd, $width, $height, $srcWidth, $srcH);
        } else { //обрезаем по ширине
            $srcWd = 0.5*($w0 - $width)*($srcWidth/$w0);
            $srcW = $srcWidth - 2*$srcWd;
            
            imagecopyresampled($resImage, $srcImage, 0, 0, $srcWd, 0, $width, $height, $srcW, $srcHeight);
        }
    }
    
    
    /*
     if ( $height > $srcHeight ) {
     //imagecopyresampled($resImage, $srcImage, 0, 0, (0.5)*(($srcWidth/$srcHeight)*$height-$width), 0, ($srcWidth/$srcHeight)*$height, $height, $srcWidth, $srcHeight);
     imagecopyresampled($resImage, $srcImage, (-0.5)*(($srcWidth/$srcHeight)*$height-$width), 0, 0, 0, ($srcWidth/$srcHeight)*$height, $height, $srcWidth, $srcHeight);
     //echo "<pre>".print_r(array($path, $srcWidth, $srcHeight, $width, $height, ($srcWidth/$srcHeight)*$height, (0.5)*(($srcWidth/$srcHeight)*$height-$width), ($srcWidth/$srcHeight)*$height), true)."</pre>";
     } else {
     imagecopyresampled($resImage, $srcImage, 0, 0, $wd, $hd, $width, $height, $width, $height);
     //imagecopyresized($dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
     }*/
    //imagecopyresized($resImage, $srcImage, 0, 0, (0.5)*(($srcWidth/$srcHeight)*$height-$width), 0, ($srcWidth/$srcHeight)*$height, $height, $srcWidth, $srcHeight);
    
    $border = 0; $border_color = '';
    foreach ($params as $key=>$val) {
        switch ($key) {
            case "border": $border = $val; break;
            case "border-color":
                $border_color = $val;
                imagerectangle($resImage, 0, 0, $width-1, $height-1, $border_color);
                //imagefill($resImage, 0, 0, $border_color); //0xffffff
                break;
        }
    }
    
    foreach ($params as $k=>$p) {
        switch (strtoupper($k)) {
            case "BRIGHTNESS":
                imagefilter($resImage, IMG_FILTER_BRIGHTNESS, $p);
                break;
        }
    }
    
    
    return ImageJPEG($resImage, $npath, 100); // 100 - максимальное качество
    
}

function pngImage($path, $npath, $width, $height, $params = array()) {
    if ( !(@fopen($path, "r")) ) {
        echo $path." - фото не найдено";
        //echo '<pre>'.print_r($ex).'</pre>';
        return false;
    }
    
    
    switch ( strtolower(strrchr($path, '.')) ){
        case ".jpg":
        case ".jpeg":
            $srcImage = @ImageCreateFromJPEG($path);
            break;
        case ".gif":
            $srcImage = @ImageCreateFromGIF($path);
            break;
        case ".png":
            $srcImage = @ImageCreateFromPNG($path);
            break;
    }
    
    $srcWidth = ImageSX($srcImage);
    $srcHeight = ImageSY($srcImage);
    
    if ($height && $srcHeight < $height ) {
        $height = $srcHeight;
    }
    if ($width && $srcWidth < $width) {
        $width = $srcWidth;
    }
    
    $h0 = ($srcHeight/$srcWidth)*$width;
    $w0 = ($srcWidth/$srcHeight)*$height;
    
    if ( !($height) ) $height = $h0;
    if ( !($width) ) $width = $w0;
    
    if ( $h0 >= $height ) {
        $width = $w0;
    } elseif ( $w0 >= $width ) {
        $height = $h0;
    }
    
    
    $resImage = ImageCreateTrueColor($width, $height);
    imageAlphaBlending($resImage, false);
    imageSaveAlpha($resImage, true);
    imagefill($resImage, 0, 0, imagecolorallocatealpha($resImage,0, 0, 0, 127));
    
    $wd = ($srcWidth-$width)/2; //if ($wd<2) $wd=0;
    $hd = ($srcHeight-$height)/2; //if ($hd<2) $hd=0;
    
    if ( $srcWidth < $width && $srcHeight < $height ) {
        imagecopyresampled($resImage, $srcImage, (-1)*$wd, (-1)*$hd, 0, 0, $srcWidth, $srcHeight, $srcWidth, $srcHeight);
    } else {
        
        
        if ( $srcWidth/$srcHeight >= $width/$height ) { #если у источника ширина больше то выравнивание по ширине
            $dstWidth = $width;
            $dstHeight = ($width/$srcWidth)*$srcHeight;
        } else { #иначе по высоте
            $dstWidth = ($height/$srcHeight)*$srcWidth;
            $dstHeight = $height;
        }
        
        
        imagecopyresampled($resImage, $srcImage, 0, 0, 0, 0, $dstWidth, $dstHeight, $srcWidth, $srcHeight);
        /*
         if ( $h0 >= $height ) { //обрезаем по высоте
         $srcHd =  0.5*($h0 - $height)*($srcHeight/$h0);
         $srcH = $srcHeight - 2*$srcHd;
         
         imagecopyresampled($resImage, $srcImage, 0, 0, 0, $srcHd, $width, $height, $srcWidth, $srcH);
         } else { //обрезаем по ширине
         $srcWd = 0.5*($w0 - $width)*($srcWidth/$w0);
         $srcW = $srcWidth - 2*$srcWd;
         
         imagecopyresampled($resImage, $srcImage, 0, 0, $srcWd, 0, $width, $height, $srcW, $srcHeight);
         }*/
        
    }
    
    
    
    foreach ($params as $k=>$p) {
        switch (strtoupper($k)) {
            case "BRIGHTNESS":
                imagefilter($resImage, IMG_FILTER_BRIGHTNESS, $p);
                break;
            case "GRAYSCALE":
                imagefilter($resImage, IMG_FILTER_GRAYSCALE, $p);
                break;
        }
    }
    //  echo '<pre>'.print_r($resImage, true).'</pre>';
    //imageCopyResampled($srcImage, $resImage, 0, 0, 0, 0, $width, $height, $srcWidth, $srcHeight);
    
    // Выводим изображение в буффер
    //imagePng($resImage);exit();
    //dbg($srcImage, true);
    
    return ImagePNG($resImage, $npath, 9); // 9 - максимальное качество
    
    
}

function sentence_split($text, $sentences) {
    $array = explode(".", $text);
    if (count($array)) {
        $ex = '';
        for ($i=0;$i<$sentences;$i++) $ex .= array_shift($array).'.';
    } else {
        $ex = $text;
    }
    return array($ex, implode(".", $array));
}

function langfix($rows) {
	$lang_prefix = langprefix();
	foreach ($rows as &$row) {
		foreach ($row as $k=>$v) {
			if (substr($k, -3) == $lang_prefix) $row->{substr($k, 0, -3)} = $v;
		}
	}
	return $rows;
}

function getORM($name = '') {
	$app = JFactory::getApplication();
	if ($name)
		$entity = $name;
	else
		$entity = $app->input->get('entity', 'default');

	require_once JPATH_ROOT.'/administrator/components/com_cck/helpers/orm.php';
	require_once JPATH_ROOT.'/administrator/components/com_cck/orm/'.$entity.'.php';
	$orm = 'CCKORM'.$entity;
	return new $orm();
}
