<?php
// no direct access
defined('_JEXEC') or die('Restricted access');
if (count($items['1']) > 0 ) :
?>
<ul class="navigation">
	<?php foreach ($items['1'] as $item) : $item_params = json_decode($item->params);?>
	<li class="navigation-item">
		<a class="navigation-link" href="<?php echo $item->href; ?>"<?php if ($item_params->{'menu-anchor_css'}) echo ' class="'.$item_params->{'menu-anchor_css'}.'"'; ?>><?php echo $item->title; ?></a>
	</li>
	<?php endforeach; ?>
</ul>
<?php endif; ?>