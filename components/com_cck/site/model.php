<?php defined('_JEXEC') or die;

class CCKJModelLegacy extends JModelLegacy {
	public $orm;

	public function __construct(array $config = array()) {
		//ORM
		$this->orm = getORM();
		parent::__construct($config);
	}
    public function getStoreId($array = array()) {
        $lang = JFactory::getLanguage();
        
        $id = ":com_cck";
        $id .= ":".$this->_context;
        $id .= ":".serialize($array);
		$id .= ":".$lang->getTag();
        
        return md5($id);
    }
    
    public function cache($store){
        $cache = JFactory::getCache('com_cck', '');
        return $cache->get( $store, 'com_cck' );
    }
    
    public function store($rows, $store) {
        $cache = JFactory::getCache('com_cck', '');
        return $cache->store( $rows, $store, 'com_cck' );
    }
}
