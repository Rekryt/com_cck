<?php
function CCKParseRoute($segments) {
	$helper = new CCKHelper();
	$app = JFactory::getApplication();
	
	$vars = array();
	$menu = JFactory::getApplication()->getMenu();
	$q = $menu->getActive()->query;
	$count = count($segments);
	$vars['query'] = $_SERVER['QUERY_STRING'];
	
	switch( $q['view']) {
		case 'cck':
			$vars['view'] = 'cck';
			$i=0; while ($seg = array_shift($segments)) { $i++;
			switch (true) {
				case $helper->check('search',$seg,$i,array("1")):
					$vars['view'] = 'search';
				break;
			}
		}
		break;
		case 'items':
			$i=0; while ($seg = array_shift($segments)) { $i++;
				switch (true) {
					case $helper->check($q['entity'],$seg,$i,array("1")):
						$vars['view'] = 'item';
						$vars['entity'] = $q['entity'];
						$vars['id'] = $helper->getItemid();
					break;
				}
			}
			//echo "<pre>".print_r(array($vars,$segments, $q), true)."</pre>"; exit();
		break;
	}

	//echo "<pre>".print_r(array($vars,$segments, $q), true)."</pre>"; exit();
	return $vars;
}