<?php defined('_JEXEC') or die;

class CCKControllerContacts extends CCKController {
	function display($cachable = false, $urlparams = array()) {
		$helper = new CCKHelper();
		$active = JFactory::getApplication()->getMenu()->getActive();

		$view = $this->getView('contacts', 'html');
		$view->setLayout('default');

		$view->assign('before', $this->params->get('contacts_before'.langprefix()));
		$view->assign('after', $this->params->get('contacts_after'.langprefix()));
		$view->assign('menu', $active);

		if (JRequest::checkToken()) {
			//google reCaptcha v3 token
			$reCaptcha = json_decode(file_get_contents_curl("https://www.google.com/recaptcha/api/siteverify", array(
				'secret'=>$this->params->get("google_recaptcha_secret"),
				'response'=>$this->input->get('gRecaptcha3_token'),
				'remoteip'=>GetIP()
			)));

			if (!$reCaptcha->success) {
				JError::raiseWarning('','Google reCAPTCHAv3 Error');
				return 0;
			}

			$mails = [];
			$data = JRequest::get('post');
			$form = $data['form'];
			if ($this->params->get('email')) $mails[] = $this->params->get('email');
			if ($this->params->get('email2')) $mails[] = $this->params->get('email2');

			if (!$form['name']) {
				$this->raiseMessage('COM_CCK_NAME_REQUIRED');
			} elseif (!$form['email']) {
				$this->raiseMessage('COM_CCK_EMAIL_REQUIRED');
			} elseif (sendmail($mails, $this->params->get('contacts_subject'), $this->params->get('contacts_text'), $form)) {
				$this->raiseMessage('COM_CCK_MAIL_SENT');
			}
		}

		$view->assign('google_recaptcha_key', $this->params->get('google_recaptcha_key'));

		$helper->setMetaData([
			"title"=>$this->params->get('contacts_title'.langprefix()),
			"description"=>$this->params->get('contacts_description'.langprefix()),
			"og:title"=>$this->params->get('contacts_title'.langprefix()),
			"og:description"=>$this->params->get('contacts_description'.langprefix()),
			"og:url"=>JURI::current(),
			"og:type"=>"website"
		]);

		if ($this->params->get('contacts_image')) {
			$thumb = generateThumb($this->params->get('contacts_image'), '800', '');
			$helper->setMetaData([
				"og:image"=>$thumb['thumb_href']
			]);
		}

		parent::display($cachable, $urlparams);
		//JFactory::getApplication()->close();
	}
}
