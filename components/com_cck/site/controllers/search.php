<?php defined('_JEXEC') or die;

class CCKControllerSearch extends CCKController {
	function display($cachable = false, $urlparams = array()) {
		$helper = new CCKHelper();
		$page = $this->input->get('page', 1, 'integer');
		$search = $this->input->get('search', '', 'string');
		$limit = 10;

		$view = $this->getView('items', 'html');
		$view->setLayout('search');

		$where = ['search'=>$search];
		$entities = ['categories','news','pages','products','publications','articles'];
		$model = $this->getModel('items');
		$pages = ceil($model->search($entities, $where, [], true) / $limit);
		$items = $model->search(
			$entities,
			array_merge($where, [
				'order'=>'date_create DESC'
			]),
			[
				($page-1)*$limit,
				$limit
			]
		);

		$view->assign('items', $items);
		$view->assign('page', $page);
		$view->assign('pages', $pages);
		$view->assign('search', $search);

		$helper->setMetaData([
			"title"=>$this->params->get('main_title'.langprefix()),
			"description"=>$this->params->get('main_description'.langprefix()),
			"og:title"=>$this->params->get('main_title'.langprefix()),
			"og:description"=>$this->params->get('main_description'.langprefix()),
			"og:url"=>JURI::current(),
			"og:type"=>"website"
		]);

		if ($this->params->get('main_image')) {
			$thumb = generateThumb($this->params->get('main_image'), '800', '');
			$helper->setMetaData([
				"og:image"=>$thumb['thumb_href']
			]);
		}

		return $view->display();
		//parent::display($cachable, $urlparams);
		//JFactory::getApplication()->close();
	}
}
