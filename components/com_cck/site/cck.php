<?php
//Защита от прямого обращения к скрипту
defined('_JEXEC') or die;

/*
//sessions
jimport( 'joomla.session.session' );
 
//load tables
JTable::addIncludePath(JPATH_COMPONENT.'/tables');
 
//load classes
JLoader::registerPrefix('ET', JPATH_COMPONENT);
 
//Load plugins
//JPluginHelper::importPlugin('et');
*/

//application
$app = JFactory::getApplication();

require_once (JPATH_COMPONENT."/lib/functions.php");
require_once (JPATH_COMPONENT."/controller.php");
require_once (JPATH_COMPONENT."/model.php");
require_once (JPATH_COMPONENT."/helpers/helper.php");

// Require specific controller if requested
if($controller = $app->input->get('view','cck')) {
	require_once (JPATH_COMPONENT.'/controllers/'.$controller.'.php');
}

// Create the controller
$classname = 'CCKController'.$controller;
$controller = new $classname();

//Выполнить задачу запроса
$controller->execute(JRequest::getCmd('task'));

//Переадресация
$controller->redirect();
