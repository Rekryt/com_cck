<?php defined('_JEXEC') or die;

class CCKViewItem extends JViewLegacy {
	protected $state;
	protected $item;
	protected $form;

	private $orm;
	public $entity;
	public $relation = [];
	public $relation_data = [];

	public function __construct(array $config = array()) {
		//ORM
		$this->orm = getORM();
		parent::__construct($config);
	}

	public function display($tpl = null) {
		$this->state  = $this->get('State');
		$this->item    = $this->get('Item');
		$this->form    = $this->get('Form');
		$this->entity = $this->orm->name_entities;

		$model = $this->getModel();
		foreach ($this->orm->relations as $field=>$relation) {
			$this->relation[$field] = $model->source($relation);
			if (isset($relation['multiple_entity']))
				$this->relation_data[$field] = $this->item->id ? $model->getMultipleData($relation, $this->item->id) : [];

		}
		//$this->groups = $model->getGroups();

		// Сообщение об ошибке.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	//Создание новых данных
	protected function addToolbar() {
		JFactory::getApplication()->input->set('hidemainmenu', true);

		$user    = JFactory::getUser();
		$userId    = $user->get('id');
		$isNew    = ($this->item->id == 0);
		//$checkedOut  = !($this->item->checked_out == 0 || $this->item->checked_out == $user->get('id'));
		$canDo    = CCKHelper::getActions();

		JToolbarHelper::title($this->orm->params['item']['title'], 'priceleafs.png');

		// Вывод кнопок навигации
		if (!false && ($canDo->get('core.edit')||(count($user->getAuthorisedCategories('com_cck', 'core.create')))))  {
			JToolbarHelper::apply('item.apply');
			JToolbarHelper::save('item.save');
			//JToolbarHelper::save2copy('client.save2copy');
		}

		if (empty($this->item->id)) {
			JToolbarHelper::cancel('item.cancel');
		}
		else {
			JToolbarHelper::cancel('item.cancel', 'JTOOLBAR_CLOSE');
		}
		//Кнопка помощи, в данном случае я указал страницу куда ссылаться. При нажатии кнопки появится модальное окно с моим сайтом.
		JToolbarHelper::divider();
		$help_url  = '#help';
		JToolbarHelper::help('Помощь', false, $help_url );
	}
}
