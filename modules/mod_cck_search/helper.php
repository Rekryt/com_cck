<?php
defined('_JEXEC') or die('Restricted access');

// Используем route.php для создания ссылок на контент
require_once (JPATH_SITE.'/'.'components'.'/'.'com_cck'.'/'.'router.php'); //vb helper

class modCCKSearchHelper {
    function getList() {
        $db =& JFactory::getDBO();
        $db->setQuery("
            SELECT *
            FROM #__vp_banners
            WHERE state = 1
            ORDER BY ordering"
        );
        $rows = langfix($db->loadObjectList());
        return $rows;
    }
}