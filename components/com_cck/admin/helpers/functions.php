<?php
function dbg($var, $exit = false) {
    echo "<pre>".print_r($var, true)."</pre>";
    if ($exit) exit();
}

function getORM($name = '') {
	$app = JFactory::getApplication('administrator');
	if ($name)
		$entity = $name;
	else
		$entity = $app->input->get('entity', 'default');

	require_once JPATH_COMPONENT.'/orm/'.$entity.'.php';
	$orm = 'CCKORM'.$entity;
	return new $orm();
}

function generateThumb ($file, $width, $height, $cacheSubdir="thumbs/",$action="crop",$params=array()) {

    if (!(substr($file, 0, 1) == "/" || substr($file, 0, 1) == "\\")) {
        $file = "/".$file;
    }

    $cache_dir = "/img/";

    if ( !is_dir(JPATH_ROOT.$cache_dir.$cacheSubdir) ) {
        mkdir(JPATH_ROOT.$cache_dir.$cacheSubdir, 0755);
    }
    $file = iconv('UTF-8', 'windows-1251', $file);
    $dir = explode('/',$file);
    $file = array_pop($dir); $res = '';
    for ($i=1;$i<count($dir);$i++) {
        $res .= '/'.$dir[$i];
    }
    $dir = $res."/";

    $file_parts = explode('.',$file);
    $ext = strtolower(array_pop($file_parts));
    if ($ext!="png") {
        $ext = "jpg";
    }

    if ($action == "png") {
        $ext = "png";
    }


    $file_name = implode('.',$file_parts);
    $file_size = filesize(JPATH_ROOT.$dir.$file);


    $thumb_name = md5($dir."-".$file_name."-".$action."-".$file_size."-".serialize($params))."-".$width."x".$height.".".$ext;

    $src_title = urldecode(htmlspecialchars($file_name));
    $thumb_title = urldecode(htmlspecialchars($thumb_name));


    /*$log_ex = time()." ".$src_title." ".$thumb_title." ".JPATH_ROOT.$cache_dir.$cacheSubdir.$thumb_name." ".(@fopen(JPATH_ROOT.$cache_dir.$cacheSubdir.$thumb_name, "r"))."\n";
     $log = fopen(JPATH_ROOT.$cache_dir."thumbs.log", "a");
     fputs($log, $log_ex, strlen($log_ex));
     fclose($log);*/

    //создание thumb-а
    //if ( !is_file(JPATH_ROOT.$cache_dir.$cacheSubdir.$thumb_name) ) {

    if ( !(@fopen(JPATH_ROOT.$cache_dir.$cacheSubdir.$thumb_name, "r")) ) {
        switch ($action) {
            case "png":
                $ex = pngImage(JPATH_ROOT.$dir.$file, JPATH_ROOT.$cache_dir.$cacheSubdir.$thumb_name, $width, $height, $params);
                break;
            case "crop":
            default:
                $ex = cropImage(JPATH_ROOT.$dir.$file, JPATH_ROOT.$cache_dir.$cacheSubdir.$thumb_name, $width, $height, $params);
                break;
        }
    }

    $thumbs['name'] = $file;
    $thumbs['src_href'] = $dir.$file;
    $thumbs['thumb_href'] = $cache_dir.$cacheSubdir.$thumb_title;
    return $thumbs;
}

function cropImage($path, $npath, $width, $height, $params = array()) {
    if ( !(@fopen($path, "r")) ) {
        //echo $path." - фото не найдено";
        return false;
    }
    switch ( strtolower(strrchr($path, '.')) ){
        case ".jpg":
        case ".jpeg":
            $srcImage = @ImageCreateFromJPEG($path);
            break;
        case ".gif":
            $srcImage = @ImageCreateFromGIF($path);
            break;
        case ".png":
            $srcImage = @ImageCreateFromPNG($path);
            break;
    }

    $srcWidth = ImageSX($srcImage);
    $srcHeight = ImageSY($srcImage);

    if ($height && $srcHeight < $height ) {
        $height = $srcHeight;
    }
    if ($width && $srcWidth < $width) {
        $width = $srcWidth;
    }

    $h0 = ($srcHeight/$srcWidth)*$width + 0;
    $w0 = ($srcWidth/$srcHeight)*$height + 0;

    if ( !($height) ) $height = $h0;
    if ( !($width) ) $width = $w0;

    $resImage = ImageCreateTrueColor($width, $height);
    imagefill($resImage, 0, 0, 0xffffff);

    $wd = ($srcWidth-$width)/2; //if ($wd<2) $wd=0;
    $hd = ($srcHeight-$height)/2; //if ($hd<2) $hd=0;

    if ( $srcWidth < $width && $srcHeight < $height ) {
        imagecopyresampled($resImage, $srcImage, (-1)*$wd, (-1)*$hd, 0, 0, $srcWidth, $srcHeight, $srcWidth, $srcHeight);
    } else {
        if ( $h0 >= $height ) { //обрезаем по высоте
            $srcHd =  0.5*($h0 - $height)*($srcHeight/$h0);
            $srcH = $srcHeight - 2*$srcHd;

            imagecopyresampled($resImage, $srcImage, 0, 0, 0, $srcHd, $width, $height, $srcWidth, $srcH);
        } else { //обрезаем по ширине
            $srcWd = 0.5*($w0 - $width)*($srcWidth/$w0);
            $srcW = $srcWidth - 2*$srcWd;

            imagecopyresampled($resImage, $srcImage, 0, 0, $srcWd, 0, $width, $height, $srcW, $srcHeight);
        }
    }


    /*
     if ( $height > $srcHeight ) {
     //imagecopyresampled($resImage, $srcImage, 0, 0, (0.5)*(($srcWidth/$srcHeight)*$height-$width), 0, ($srcWidth/$srcHeight)*$height, $height, $srcWidth, $srcHeight);
     imagecopyresampled($resImage, $srcImage, (-0.5)*(($srcWidth/$srcHeight)*$height-$width), 0, 0, 0, ($srcWidth/$srcHeight)*$height, $height, $srcWidth, $srcHeight);
     //echo "<pre>".print_r(array($path, $srcWidth, $srcHeight, $width, $height, ($srcWidth/$srcHeight)*$height, (0.5)*(($srcWidth/$srcHeight)*$height-$width), ($srcWidth/$srcHeight)*$height), true)."</pre>";
     } else {
     imagecopyresampled($resImage, $srcImage, 0, 0, $wd, $hd, $width, $height, $width, $height);
     //imagecopyresized($dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
     }*/
    //imagecopyresized($resImage, $srcImage, 0, 0, (0.5)*(($srcWidth/$srcHeight)*$height-$width), 0, ($srcWidth/$srcHeight)*$height, $height, $srcWidth, $srcHeight);

    $border = 0; $border_color = '';
    foreach ($params as $key=>$val) {
        switch ($key) {
            case "border": $border = $val; break;
            case "border-color":
                $border_color = $val;
                imagerectangle($resImage, 0, 0, $width-1, $height-1, $border_color);
                //imagefill($resImage, 0, 0, $border_color); //0xffffff
                break;
        }
    }

    foreach ($params as $k=>$p) {
        switch (strtoupper($k)) {
            case "BRIGHTNESS":
                imagefilter($resImage, IMG_FILTER_BRIGHTNESS, $p);
                break;
        }
    }


    return ImageJPEG($resImage, $npath, 100); // 100 - максимальное качество

}

function pngImage($path, $npath, $width, $height, $params = array()) {
    if ( !(@fopen($path, "r")) ) {
        echo $path." - фото не найдено";
        //echo '<pre>'.print_r($ex).'</pre>';
        return false;
    }


    switch ( strtolower(strrchr($path, '.')) ){
        case ".jpg":
        case ".jpeg":
            $srcImage = @ImageCreateFromJPEG($path);
            break;
        case ".gif":
            $srcImage = @ImageCreateFromGIF($path);
            break;
        case ".png":
            $srcImage = @ImageCreateFromPNG($path);
            break;
    }

    $srcWidth = ImageSX($srcImage);
    $srcHeight = ImageSY($srcImage);

    if ($height && $srcHeight < $height ) {
        $height = $srcHeight;
    }
    if ($width && $srcWidth < $width) {
        $width = $srcWidth;
    }

    $h0 = ($srcHeight/$srcWidth)*$width;
    $w0 = ($srcWidth/$srcHeight)*$height;

    if ( !($height) ) $height = $h0;
    if ( !($width) ) $width = $w0;

    if ( $h0 >= $height ) {
        $width = $w0;
    } elseif ( $w0 >= $width ) {
        $height = $h0;
    }


    $resImage = ImageCreateTrueColor($width, $height);
    imageAlphaBlending($resImage, false);
    imageSaveAlpha($resImage, true);
    imagefill($resImage, 0, 0, imagecolorallocatealpha($resImage,0, 0, 0, 127));

    $wd = ($srcWidth-$width)/2; //if ($wd<2) $wd=0;
    $hd = ($srcHeight-$height)/2; //if ($hd<2) $hd=0;

    if ( $srcWidth < $width && $srcHeight < $height ) {
        imagecopyresampled($resImage, $srcImage, (-1)*$wd, (-1)*$hd, 0, 0, $srcWidth, $srcHeight, $srcWidth, $srcHeight);
    } else {


        if ( $srcWidth/$srcHeight >= $width/$height ) { #если у источника ширина больше то выравнивание по ширине
            $dstWidth = $width;
            $dstHeight = ($width/$srcWidth)*$srcHeight;
        } else { #иначе по высоте
            $dstWidth = ($height/$srcHeight)*$srcWidth;
            $dstHeight = $height;
        }


        imagecopyresampled($resImage, $srcImage, 0, 0, 0, 0, $dstWidth, $dstHeight, $srcWidth, $srcHeight);
        /*
         if ( $h0 >= $height ) { //обрезаем по высоте
         $srcHd =  0.5*($h0 - $height)*($srcHeight/$h0);
         $srcH = $srcHeight - 2*$srcHd;

         imagecopyresampled($resImage, $srcImage, 0, 0, 0, $srcHd, $width, $height, $srcWidth, $srcH);
         } else { //обрезаем по ширине
         $srcWd = 0.5*($w0 - $width)*($srcWidth/$w0);
         $srcW = $srcWidth - 2*$srcWd;

         imagecopyresampled($resImage, $srcImage, 0, 0, $srcWd, 0, $width, $height, $srcW, $srcHeight);
         }*/

    }



    foreach ($params as $k=>$p) {
        switch (strtoupper($k)) {
            case "BRIGHTNESS":
                imagefilter($resImage, IMG_FILTER_BRIGHTNESS, $p);
                break;
            case "GRAYSCALE":
                imagefilter($resImage, IMG_FILTER_GRAYSCALE, $p);
                break;
        }
    }
    //  echo '<pre>'.print_r($resImage, true).'</pre>';
    //imageCopyResampled($srcImage, $resImage, 0, 0, 0, 0, $width, $height, $srcWidth, $srcHeight);

    // Выводим изображение в буффер
    //imagePng($resImage);exit();
    //dbg($srcImage, true);

    return ImagePNG($resImage, $npath, 9); // 9 - максимальное качество


}

function sendmail($emails, $subject, $text, $data = array(), $file = '') {
	$config = JFactory::getConfig();

	$data['fromname'] = $config->get('fromname');
	$data['mailfrom'] = $config->get('mailfrom');
	$data['sitename'] = $config->get('sitename');
	$data['siteurl'] = JUri::base();

	$pattern = array();
	foreach ($data as $key=>$val) {
		$pattern["{".$key."}"] = $val;
	}

	#Отправка почты
	$emailSubject = strtr($subject, $pattern);
	$emailBody = strtr($text, $pattern);

	$mailer = JFactory::getMailer();
	$mailer->setSender(array($data['mailfrom'], $data['fromname']));

	if ($file) {
		if (substr($file, 0, 1) != '/') $file = '/'.$file;
		$mailer->addAttachment(JPATH_ROOT.$file);
	}

	foreach ($emails as $email) {
		$mailer->addRecipient($email);
	}

	$mailer->setSubject($emailSubject);
	$mailer->setBody($emailBody);

	$mailer->isHTML(true);
	return $mailer->Send();
}