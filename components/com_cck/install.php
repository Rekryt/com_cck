<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.installer.installer');
jimport('joomla.installer.helper');

/**
* Method to install the component
*
* @param mixed $parent The class calling this method
* @return void
*/
function install($parent)
{
    echo JText::_('Component install successful');
}

/**
* Method to update the component
*
* @param mixed $parent The class calling this method
* @return void
*/
function update($parent)
{
    echo JText::_('Component update successful');
}

/**
* Method to run before an install/update/uninstall method
*
* @param mixed $parent The class calling this method
* @return void
*/
/*
function preflight($type, $parent)
{
    ...
}
 
function postflight($type, $parent)
{
    ...
}
*/