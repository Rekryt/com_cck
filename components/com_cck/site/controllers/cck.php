<?php defined('_JEXEC') or die;

class CCKControllerCCK extends CCKController {
	function display($cachable = false, $urlparams = array()) {
		$helper = new CCKHelper();

		$view = $this->getView('cck', 'html');
		$view->setLayout('default');

		$model = $this->getORMModel('items', getORM('news'));
		$news = [];
		$news[] = $model->get(
			[
				'news.categories_id'=>$this->params->get('main_news1', 1),
				'order'=>'date_create DESC'
			],
			[$this->params->get('main_news_limit', 5)]
		);
		$news[] = $model->get(
			[
				'news.categories_id'=>$this->params->get('main_news2', 1),
				'order'=>'ordering ASC'
			],
			[$this->params->get('main_news_limit', 5)]
		);

		$view->assign('news', $news);
		$view->assign('news_href', [
			getSEFlink('index.php?option=com_cck&view=items&entity=news&categories_id='.$this->params->get('main_news1', 1)),
			getSEFlink('index.php?option=com_cck&view=items&entity=news&categories_id='.$this->params->get('main_news2', 1))
		]);
		$view->assign('text', $this->params->get('main_text'.langprefix()));

		$helper->setMetaData([
			"title"=>$this->params->get('main_title'.langprefix()),
			"description"=>$this->params->get('main_description'.langprefix()),
			"og:title"=>$this->params->get('main_title'.langprefix()),
			"og:description"=>$this->params->get('main_description'.langprefix()),
			"og:url"=>JURI::current(),
			"og:type"=>"website"
		]);

		if ($this->params->get('main_image')) {
			$thumb = generateThumb($this->params->get('main_image'), '800', '');
			$helper->setMetaData([
				"og:image"=>$thumb['thumb_href']
			]);
		}

		parent::display($cachable, $urlparams);
		//JFactory::getApplication()->close();
	}
}
