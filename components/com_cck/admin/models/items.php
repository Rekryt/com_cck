<?php defined('_JEXEC') or die;

class CCKModelItems extends JModelList {
	private $orm;

	public function __construct($config = array()) {
		//ORM
		$this->orm = getORM();

		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = [];
			foreach ($this->orm->params['items']['list'] as $field)
				$config['filter_fields'][] = isset($field['sort_field']) ? $field['sort_field'] : $field['field'];
		}

		parent::__construct($config);
	}

	public function setError($error) {
		$db = JFactory::getDBO();
		if (strpos($error, "doesn't exist") > 0) {
			foreach ($this->orm->create() as $table) {
				$db->setQuery($table);
				$db->query();
				JError::raiseNotice(100,"<pre>$table</pre>");
			}
			JError::raiseNotice(100,"Сущность создана");
			$error = 'Требуется обновление страницы';
		}

		parent::setError($error);
	}

	protected function populateState($ordering = null, $direction = null) {
		// Загрузите состояние фильтра.
		$search = $this->getUserStateFromRequest($this->context.'.filter.'.$this->orm->name_entities."_search", 'filter_'.$this->orm->name_entities.'_search');
		$this->setState('filter.'.$this->orm->name_entities.'_search', $search);

		foreach ($this->orm->params['items']['filters'] as $field=>$filter) {
			$p = $this->orm->name_entities.'_'.$field;
			$value = $this->getUserStateFromRequest($this->context.'.filter.'.$p, 'filter_'.$p, '', 'string');
			$this->setState('filter.'.$p, $value);
		}

		// Загрузка параметров
		$params = JComponentHelper::getParams('com_cck');
		$this->setState('params', $params);

		// Список информации о состоянии
		parent::populateState($this->orm->params['items']['order'], $this->orm->params['items']['dir']);
	}

	//Родные фильтры joomla с помощью них создаётся поиск и фильтрация опубликованных и неопубликованных данных
	protected function getStoreId($id = '')
	{
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');
		return parent::getStoreId($id);
	}

	protected function getListQuery() {
		//dbg($this->orm, true);

		// Создайте новый объект запроса.
		$db    = $this->getDbo();
		$query  = $db->getQuery(true);
		//$user  = JFactory::getUser();
		$alias = $this->orm->name_entities;
		// Выбираем нужные поля из таблицы
		foreach (array_keys($this->orm->list) as $field)
			$query->select("$alias.".$field);

		$query->from('#__cck_'.$this->orm->name_table." AS $alias");

		$i = 1;
		foreach ($this->orm->relations as $key=>$relation) {
			if (!$relation['list']) continue;
			//$p = "r".$i; $i++;
			$p = $relation['entity'];
			if (isset($relation['multiple_entity'])) {
				$query->leftJoin('#__cck_'.$relation['multiple_entity']." rel_$p ON rel_$p.".$relation['multiple_entity_field']." = a.".$relation['multiple_key']);
				$query->leftJoin('#__cck_'.$relation['entity']." $p ON $p.".$relation['field']." = rel_$p.".$relation['multiple_field']);
			} else {
				$query->leftJoin('#__cck_'.$relation['entity']." $p ON $p.".$relation['field']." = ".$alias.".".$key);
			}
			foreach ($relation['value'] as $item)
				$query->select("$p.".$item." as ".$relation['entity']."_".$item);
		}

		//Фильтры поиска
		foreach ($this->orm->params['items']['filters'] as $field=>$filter) {
			$value = $this->getState('filter.'.$alias.'_'.$field);
			switch ($filter['type']) {
				case "search":
					if (!empty($value)) {
						$search = [];
						foreach ($filter['field'] as $f) {
							if (stripos($value, 'id:') === 0) {
								$query->where($alias.".".$f." = ".(int) substr($field, 3));
							} else {
								$search[] = $alias.".".$f." LIKE ".$db->Quote('%'.$db->escape($value, true).'%');
							}
						}
						$query->where("(".implode(' OR ', $search).")");
					}
				break;
				case "select":
				default:
					if (is_numeric($value)) {
						$query->where($alias.'.'.$field.' = '.(int) $value);
					} elseif ($value) {
						$query->where($alias.'.'.$field.' = "'. addslashes($value).'"');
					}
				break;
			}

		}

		// сортировка
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn  = $this->state->get('list.direction');
		/*if ($orderCol == 'a.ordering' || $orderCol == 'category_title') {
			$orderCol = 'a.name_ru '.$orderDirn.', a.ordering';
		}*/

		$query->order($db->escape($orderCol.' '.$orderDirn));
		//dbg((string) $query, true);
		return $query;
	}
	
	public function saveorder($entity, $update) {
		$db    = $this->getDbo();
		$ids = [];
		foreach ($update as $key=>$data) {
			foreach ($data as $id=>$text) {
				if (!isset($ids[$id])) $ids[$id] = [];
				$ids[$id][] = "`$key` = '".addslashes($text)."'";
			}
		}
		
		foreach ($ids as $id=>$fields) {
			$query = "UPDATE #__cck_".$entity." SET ".implode(",", $fields)." WHERE id = '".$id."'";
			$db->setQuery($query);
			$db->execute();
		}
		
		return 1;
	}
	
	/*function delete($id) {
		$db    = $this->getDbo();
	
		foreach ($id as $k=>$p) {
			$query = "DELETE FROM #__cck_items WHERE id = '".$p."'";
			$db->setQuery($query);
			$db->execute();
		}
	
		return 1;
	}*/

	public function source($source) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$value = is_array($source['value']) ? $source['value'][0] : $source['value'];
		$query
			->select($source['field'].','.$value)
			->from('#__cck_'.$source['entity']);

		$db->setQuery($query);
		$messages = $db->loadObjectList();

		$options = array();
		if ($messages)
			foreach($messages as $message)
				$options[] = JHtml::_('select.option', $message->{$source['field']}, $message->{$value});

		return $options;
	}

	function addItems($groups_id, $data){
		//Подключение к бд joomla
		$db = $this->getDbo();

		$chunks = array_chunk($data, 20);
		foreach ($chunks as $items) {
			if (count($items) > 0) {
				$values = array();
				foreach ($items as $item) $values[] = "('".addslashes($groups_id)."','".$item."')";

				$query = "
					INSERT INTO #__et_items
					(`groups_id`,`data`)
					VALUES
					".implode(",", $values)."
				";
				$db->setQuery($query);
				$res = $db->query();
			}
		}

		return true;
	}

	function update($entity, $field, $id, $value, $raw_value = false) {
		$db = $this->getDbo();

		$query = "
			UPDATE `#__cck_".$entity."`
			SET `".$field."` = ".($raw_value ? $value : "'".addslashes($value)."'")."
			WHERE id = '".addslashes($id)."'";
		$db->setQuery($query);

		$res = $db->query();
		return true;
	}

}
