<?php
class CCKORMDefault extends CCKORModel {
	public $name_table 			= '';	//имя таблицы
	public $name_entities 		= 'default';	//имя сущности
	public $name_entity 		= '';	//имя экземпляра сущности

	public $params 	= [
		'items'=>[	//параметры страницы списка элементов
			'title'=>'AZT-Pharma'	//заголовок раздела управления списком элементов
		]
	];
}