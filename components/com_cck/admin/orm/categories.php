<?php
class CCKORMCategories extends CCKORModel {
	public $name_table 			= 'categories';	//имя таблицы
	public $name_entities 		= 'categories';	//имя сущности
	public $name_entity 		= 'category';	//имя экземпляра сущности

	public $params 	= [
		'items'=>[	//параметры страницы списка элементов
			'title'=>'Категории новостей',	//заголовок раздела управления списком элементов
			'list'=>[	//список полей
				'id'				=>	['field'=>'id','sort'=>true,'link'=>true,'width'=>'1%'],
				//'image_ru'			=>  ['field'=>'image_ru','sort'=>false,'link'=>true,'type'=>'photo'],
				'name_ru'			=>	['field'=>'name_ru','sort'=>true,'link'=>true],
				'state_ru'			=>	['field'=>'state_ru','sort'=>true,'type'=>'checker','width'=>'1%'],
				'name_en'			=>	['field'=>'name_en','sort'=>true,'link'=>true],
				'state_en'			=>	['field'=>'state_en','sort'=>true,'type'=>'checker','width'=>'1%'],
				'date_create'		=> 	['field'=>'date_create','sort'=>true],
				'ordering'			=>	['field'=>'ordering','sort'=>true,'type'=>'text','width'=>'1%']
			],
			'order'=>'categories.id',	//начальная сортировка
			'dir'=>'ASC',	//начальное направление сортировки
			'filters'=>[	//фильтры
				'search'=>[	//фильтр поиска
					'type'=>'search',	//тип фильтра search - текстовое поле поиска
					'field'=>['name_ru','name_en'],	//список полей по которым проводится поиск
					'label'=>"Поиск"	//заголовок поля
				],
				'state_ru'=>[	//фильтр состояния публикации
					'type'=>'select',	//тип фильтра
					'field'=>'state',	//поле фильтрации
					'label'=>"- Выбор состояния (RU) -",	//заголовок поля
					'options'=>[	//список параметров
						'1'=>'Опубликовано',
						'0'=>'Не опубликовано'
					]
				],
				'state_en'=>[	//фильтр состояния публикации
					'type'=>'select',	//тип фильтра
					'field'=>'state',	//поле фильтрации
					'label'=>"- Выбор состояния (EN) -",	//заголовок поля
					'options'=>[	//список параметров
						'1'=>'Опубликовано',
						'0'=>'Не опубликовано'
					]
				]
			],
			'toolbar'=>[	//верхняя панель управления
				[
					'type'=>'add',	//тип элемента управления
					'label'=>'Создать'	//наименование элемента управления
				], [
					'type'=>'edit',
					'label'=>'Создать'
				], [
					'type'=>'check',	//тип check - устанавливает значение поля как '1'
					'label'=>'Опублировать',	//наименование элемента управления
					'field'=>'state_ru'	//имя поля управления
				], [
					'type'=>'uncheck',	//тип uncheck - устанавливает значение поля как '0'
					'label'=>'Снять с публикации',	//наименование элемента управления
					'field'=>'state_ru'	//имя поля управления
				], [
					'type'=>'delete',
					'label'=>'Удалить'
				]
			]
		],
		'item'=>[	//параметры страницы элемента
			'title'=>'Категория',	//заголовок
			'form'=>[	//поля формы
				'Общее'		=>	['id', 'name_ru', 'pseudoname_ru', 'name_en', 'pseudoname_en', 'date_create', 'date_modify'],
				'Русский'	=>	['state_ru', 'title_ru', 'image_ru', 'text_ru', 'description_ru'],
				'English'	=>	['state_en', 'title_en', 'image_en', 'text_en', 'description_en'],
			]
		],
		'menu'=>[ 	//параметры субменю
			'order' => 1
		]
	];

	function __construct($if_not_exists = false) {
		$this->addFields([
			'id'=>[
				'params'		=>	[
					'label'			=>	'ID',
					'type'			=>	'hidden'
				],
				'type'			=>	'int(11)',
				'null'			=>	'NOT NULL',
				'default'		=>	'',
				'autoincrement'	=>	true
			],
			'state_ru'=>[
				'params'		=>	[
					'label'			=>	'JSTATUS',
					'type'			=>	'list',
					'class'			=> 	'inputbox',
					'size'			=> 	'1',
					'default' 		=> 	'1',
					'options' 		=> 	[
						'1'				=> 	'Опубликовано',
						'0'				=> 	'Не опубликовано'
					]
				],
				'type'			=>	'tinyint(3)',
				'null'			=>	'NOT NULL',
				'default'		=>	'1'
			],
			'state_en'=>[
				'params'		=>	[
					'label'			=>	'JSTATUS',
					'type'			=>	'list',
					'class'			=> 	'inputbox',
					'size'			=> 	'1',
					'default' 		=> 	'1',
					'options' 		=> 	[
						'1'				=> 	'Опубликовано',
						'0'				=> 	'Не опубликовано'
					]
				],
				'type'			=>	'tinyint(3)',
				'null'			=>	'NOT NULL',
				'default'		=>	'1'
			],
			'ordering'=>[
				'params'		=>	[
					'label'			=>	'Порядок',
					'type'			=>	'hidden'
				],
				'type'			=>	'int(11)',
				'null'			=>	'NOT NULL',
				'default'		=>	'1'
			],
			'name_ru'=>[
				'params'		=>	[
					'label'			=>	'Наименование (RU)',
					'type'			=>	'text',
					'size'			=>	'60',
					'default'		=>	'',
					'required'		=>	true
				],
				'type'			=>	'varchar(255)',
				'null'			=>	'NOT NULL',
				'default'		=>	''
			],
			'pseudoname_ru'=>[
				'params'		=>	[
					'label'			=>	'URL псевдоним (RU)',
					'type'			=>	'text',
					'size'			=>	'60',
					'default'		=>	'',
					'description'	=>	'Генерируется при сохранении автоматически',
					'extra'			=> 	['update'=>'pseudoname', 'field'=>'name_ru']
				],
				'type'			=>	'varchar(255)',
				'null'			=>	'NOT NULL',
				'default'		=>	''
			],
			'name_en'=>[
				'params'		=>	[
					'label'			=>	'Наименование (EN)',
					'type'			=>	'text',
					'size'			=>	'60',
					'default'		=>	'',
					'required'		=>	true
				],
				'type'			=>	'varchar(255)',
				'null'			=>	'NOT NULL',
				'default'		=>	''
			],
			'pseudoname_en'=>[
				'params'		=>	[
					'label'			=>	'URL псевдоним (EN)',
					'type'			=>	'text',
					'size'			=>	'60',
					'default'		=>	'',
					'description'	=>	'Генерируется при сохранении автоматически',
					'extra'			=> 	['update'=>'pseudoname', 'field'=>'name_en']
				],
				'type'			=>	'varchar(255)',
				'null'			=>	'NOT NULL',
				'default'		=>	''
			],
			'title_ru'=>[
				'params'		=>	[
					'label'			=>	'Заголовок страницы в браузере (RU)',
					'type'			=>	'text',
					'size'			=>	'60',
					'default'		=>	''
				],
				'type'			=>	'varchar(255)',
				'null'			=>	'NOT NULL',
				'default'		=>	''
			],
			'title_en'=>[
				'params'		=>	[
					'label'			=>	'Заголовок страницы в браузере (EN)',
					'type'			=>	'text',
					'size'			=>	'60',
					'default'		=>	''
				],
				'type'			=>	'varchar(255)',
				'null'			=>	'NOT NULL',
				'default'		=>	''
			],
			'text_ru'=>[
				'params'		=>	[
					'label'			=>	'Текст страницы (RU)',
					'type'			=>	"editor",
					'filter'		=>	"raw",
					'buttons'		=>	false,
					'height'		=>	300,
					'width'			=>	750
				],
				'type'			=>	'text',
				'null'			=>	'NULL',
				'default'		=>	'NULL'
			],
			'text_en'=>[
				'params'		=>	[
					'label'			=>	'Текст страницы (EN)',
					'type'			=>	"editor",
					'filter'		=>	"raw",
					'buttons'		=>	false,
					'height'		=>	300,
					'width'			=>	750
				],
				'type'			=>	'text',
				'null'			=>	'NULL',
				'default'		=>	'NULL'
			],
			'description_ru'=>[
				'params'		=>	[
					'label'			=>	'Meta Description (RU)',
					'type'			=>	'textarea',
					'size'			=>	'60',
					'default'		=>	''
				],
				'type'			=>	'varchar(255)',
				'null'			=>	'NOT NULL',
				'default'		=>	''
			],
			'description_en'=>[
				'params'		=>	[
					'label'			=>	'Meta Description (EN)',
					'type'			=>	'textarea',
					'size'			=>	'60',
					'default'		=>	''
				],
				'type'			=>	'varchar(255)',
				'null'			=>	'NOT NULL',
				'default'		=>	''
			],
			'image_ru'=>[
				'params'		=>	[
					'type'			=>	'media',
					'label'			=>	'Фотография&nbsp;(RU)',
					'directory'		=>	''
				],
				'type'			=>	'varchar(255)',
				'null'			=>	'NOT NULL',
				'default'		=>	''
			],
			'image_en'=>[
				'params'		=>	[
					'type'			=>	'media',
					'label'			=>	'Фотография (EN)',
					'directory'		=>	''
				],
				'type'			=>	'varchar(255)',
				'null'			=>	'NOT NULL',
				'default'		=>	''
			],
			'date_create'=>[
				'params'		=>	[
					'label'			=>	'Дата создания',
					'type'			=>	'fullDate'
				],
				'type'			=>	'timestamp',
				'null'			=>	'NOT NULL',
				'default'		=>	'CURRENT_TIMESTAMP'
			],
			'date_modify'=>[
				'params'		=>	[
					'label'			=>	'Дата последней модификации',
					'type'			=>	'raw',
					'extra'			=> 	['update'=>'current_timestamp']
				],
				'type'			=>	'timestamp',
				'null'			=>	'NOT NULL',
				'default'		=>	'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
			]
		]);
		$this->addKeys([
			'id'=>[
				'type'=>'PRIMARY'
			],
			'idx_state_ru'=>[
				'fields'=>['state_ru']
			],
			'idx_state_en'=>[
				'fields'=>['state_en']
			],
			'uni_pseudoname_ru'=>[
				'type'=>'UNIQUE',
				'fields'=>['pseudoname_ru']
			],
			'uni_pseudoname_en'=>[
				'type'=>'UNIQUE',
				'fields'=>['pseudoname_en']
			]
		]);
		return parent::__construct();
	}
}