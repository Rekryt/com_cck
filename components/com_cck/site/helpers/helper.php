<?php defined('_JEXEC') or die;

class CCKHelper {
	public $itemId;

	function getLang($query_lang = false){
		if ($query_lang) {
			$lang = $query_lang == "ru-RU" ? "_ru" : "_en";
		} else {
			$lang = JFactory::getLanguage();
			$lang = $lang->getTag() == "ru-RU" ? "_ru" : "_en";
		}
		
		return $lang;
	}
	
	function getPseudoname($obj, $query, $query_lang = 'ru-RU') {
		$db = JFactory::getDBO();
		$lang = self::getLang($query_lang);

		$q = "
			SELECT pseudoname".$lang." as pseudoname
			FROM #__cck_".addslashes($obj)."
			WHERE id = '".$query['id']."'
		";

		$db->setQuery($q);
		$row = $db->loadObjectlist();

		return $row[0]->pseudoname;
	}
	
	function escape_string($string){
		return str_replace("_","\_",str_replace(":","-",addslashes($string)));
	}

	function langprefix($reverse = false) {
		$lang = JFactory::getLanguage();
		switch ($lang->getTag()) {
			case "en-GB": $lang_prefix = "_en"; break;
			case "ru-RU":
			default: $lang_prefix = "_ru"; break;
		}
		if ($reverse && $lang_prefix == "_ru") return "_en";
		if ($reverse && $lang_prefix == "_en") return "_ru";
		return $lang_prefix;
	}
	
	function getid($obj, $pseudoname, $search_byid = true, $vars = array()) {
		$db = JFactory::getDBO();
		$lang = self::getLang();

		$q = "
			SELECT id
			FROM #__cck_".addslashes($obj)."
			WHERE pseudoname".$lang." like '".self::escape_string($pseudoname)."'
			AND state".$lang." = 1
		";

		$db->setQuery($q);
		return $db->loadObjectlist();
	}
	
	function check($obj, $segment, $level, $allowlevels = array(), $vars = array()){
		if (!in_array($level, $allowlevels)) return false;

		switch ($obj) {
			case "ajax":
			case "search":
				return $segment == $obj;
			break;

			default:
				$row = self::getid($obj, $segment, true, $vars);
				
				if (count($row)) {
					$this->itemId = $row[0]->id;
					return true;
				} else {
					return false;
				}
			break;
		}
	}
	
	function getItemid(){
		return $this->itemId;
	}
	
	
	function redirect($link){
	    header("HTTP/1.1 301 Moved Permanently");
	    header("Location: ".$link);
	    exit();
	}
	
	function set_cookie($name, $data = '', $expire = 0){ //$expire = time() + 2592000 - 1 month , $expire = 0 - session cookie
	    // Get input cookie object
	    $input = JFactory::getApplication()->input;
	    $inputCookie  = $input->cookie;
	    $config = new JConfig();
	    
	    if ($data) {
	        // Set cookie data
	        $inputCookie->set($name, $data, time() + $expire, '/', $config->cookie_domain);
	        
	    } else {
	        // Remove cookie
	        $inputCookie->set($name, null, time() - 1);
	    }
	}
	
	function get_cookie($name){
	    // Get input cookie object
	    $inputCookie  = JFactory::getApplication()->input->cookie;
	     
	    // Get cookie data
	    return $inputCookie->get($name, null);
	}
	
	function setMetaData($array = array()) {
	    $doc = JFactory::getDocument();
	    
	    foreach ($array as $key=>$val) {
	        $val = str_replace("'", '', htmlspecialchars_decode(strip_tags($val)));
	        if ($val) {
	            switch ($key) {
	                case "title":
	                    $doc->setTitle($val);
                    break;
	                case "keywords":
	                case "description":
	                    $doc->setMetaData($key, $val);
                    break;
	                default:
	                    $doc->addCustomTag('<meta property="'.$key."\" content='".$val."'>"."\n");
                    break;
	            }
	        }
	    }
	}

	function array_index($data, $id, $id2 = null) {
		$array = array();
		if ($id2 != null) {
			foreach ($data as $item) $array[$item->$id][$item->$id2] = $item;
		} else {
			foreach ($data as $item) $array[$item->$id] = $item;
		}
		return $array;
	}

	function get_input_files() {
		$files = [];
		if (isset($_FILES['file']))
			foreach ($_FILES['file']['name'] as $k=>$name)
				if ($_FILES['file']['size'][$k] && $name)
					$files[] = [
						'name'=>$name,
						'type'=>$_FILES['file']['type'][$k],
						'tmp_name'=>$_FILES['file']['tmp_name'][$k],
						'error'=>$_FILES['file']['error'][$k],
						'size'=>$_FILES['file']['size'][$k]
					];
		return $files;
	}
	
}
