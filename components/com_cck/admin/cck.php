<?php defined('_JEXEC') or die;

require_once 'controller.php';
require_once 'helpers/cck.php';
require_once 'helpers/orm.php';
require_once 'helpers/functions.php';

$app = JFactory::getApplication();

//Проверка доступа к компоненту, авторизован пользователь или нет
if (!JFactory::getUser()->authorise('core.manage', 'com_cck'))  {
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

if (!JFactory::getUser()->authorise('core.admin', 'com_cck') && $app->input->get('view', 'cck') != "cck") {
	return JError::raiseWarning(404, 'Не достаточно прав доступа');
}

//Получить экземпляр контроллер с префиксом
$controller  = JControllerLegacy::getInstance('CCK');

//Выполнение задачи task
$controller->execute($app->input->get('task'));

//Редирект контроллера, если установленно контроллером
$controller->redirect();